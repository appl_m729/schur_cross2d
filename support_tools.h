#ifndef _SUPPORT_TOOLS_H_
#define _SUPPORT_TOOLS_H_

#include <iostream>

using std::cout;
using std::endl;

///////////////////////////////////////////////////////////////////////////////

// Reallocates memory for a given array of type T
template<typename T>
bool  is_1reallocation_ok_(int & current_size, const int new_size,
                          T * & array_pointer, const int inc_factor) {

    if (current_size < new_size) {

        current_size = inc_factor * new_size;

        delete[] array_pointer; array_pointer = NULL;
        array_pointer = new T[current_size];

        if (array_pointer == NULL) {
            cout << "Can't allocate memory for maxvol array - 001\n";
            return false;
        }
    }
    return true;
}

// Suitable wrapper.
template<typename T>
bool raw_reallocate_array_is_ok_(int & buffer_size, const int new_size,
                                  T * & pArray, const int inc_factor) {
    return is_1reallocation_ok_(buffer_size, new_size, pArray, inc_factor);
}

// Reallocates memory for a given array of type T with copying
// from old array to the new one.
//template<typename T>
//bool is_copy_reallocation_ok_(int & current_size,  const int new_size,
//                          T * & array_pointer, const int inc_factor) {

//    if (current_size < new_size) {

//        current_size = inc_factor * new_size;

//        delete[] array_pointer; array_pointer = NULL;
//        array_pointer = new T[current_size];

//        if (array_pointer == NULL) {
//            cout << "Can't allocate memory for maxvol array - 001\n";
//            return false;
//        }
//    }
//    return true;
//}

///////////////////////////////////////////////////////////////////////////////

#endif  // _SUPPORT_TOOLS_H_

// End of the file

