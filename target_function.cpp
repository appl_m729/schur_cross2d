#include <iostream>
#include <iomanip>
#include <cmath>
#include "target_function.h"

using std::cout;
using std::endl;
using std::setprecision;
using std::setw;

///////////////////////////////////////////////////////////////////////////////

TargetFunction::TargetFunction():
n_rows_(1),
m_cols_(1),
ax_left_bound_(0),
bx_right_bound_(1),
ay_left_bound_(0),
by_right_bound_(1) {

}

TargetFunction::~TargetFunction() {
}

// virtual
double TargetFunction::value(const int i, const int j) const {

    const double Dx_ab = bx_right_bound_ - ax_left_bound_;
    const double Dy_ab = by_right_bound_ - ay_left_bound_;
    const double i_over_n = static_cast<double>(i) / static_cast<double>(n_rows_ - 1);
    const double j_over_m = static_cast<double>(j) / static_cast<double>(m_cols_ - 1);

    const double xi = ax_left_bound_ + Dx_ab * i_over_n;
    const double yj = ay_left_bound_ + Dy_ab * j_over_m;

//    if (i == 7 && j == 9) return 1;
//    if (i == 8 && j == 8) return 1;
//    if (i == 9 && j == 7) return 1;
//    return 0;

//    if (j > 1 || i > 1) {return 0;}
//    if (i > 2) {return j + 1;}

//    const int pow_k = i * m_cols() + j;
//    return pow(2.0, -pow_k);

//    return  i * m_cols() + j;

//    return exp(-sqrt(xi * xi + yj * yj));
//    return 1.0;
//    return (i + 1) * (j + 1);
//    return (xi + 1) * (yj + 1);
//    return xi * xi + xi * yj + 4*yj * yj;
//    return exp(-xi * xi - yj * yj);
//    return 34.0 * xi * xi + 0.117 * yj * yj;
//    return 1.0/(static_cast<double>(i) + j + 1.0);
//    return 1.0/(xi + yj + 1.0);
//    return 1.0 / sqrt(xi * xi + yj * yj + 1.0);
//    return (4.0 + xi * xi * xi)/(2.0 + sin(15.0 * xi * xi + 9.0 * yj * yj * yj));
    const double nqc = exp(cos(2 - xi + yj * yj));
    return (4.0 + xi * xi * xi + nqc)/(2.0 + sin(15.0 * xi * xi + 9.0 * yj * yj * yj));
}

bool TargetFunction::set_n_rows(const int nRows) {

    if (nRows > 1) {
        n_rows_ = nRows;
        return true;
    }
    return false;
}

bool TargetFunction::set_m_cols(const int mCols) {

    if (mCols > 1) {
        m_cols_ = mCols;
    }
    return false;
}

void TargetFunction::set_ax_left_bound(const double ax) {
    ax_left_bound_ = ax;
}

void TargetFunction::set_bx_right_bound(const double bx) {
    bx_right_bound_ = bx;
}

void TargetFunction::set_ay_left_bound(const double ay) {
    ay_left_bound_ = ay;
}

void TargetFunction::set_by_right_bound(const double by) {
    by_right_bound_ = by;
}

bool TargetFunction::are_bounds_setup_properly() const {

    if (ax_left_bound() >= bx_right_bound()) {return false;}
    if (ay_left_bound() >= by_right_bound()) {return false;}
    return true;
}

int TargetFunction::n_rows() const {
    return n_rows_;
}

int TargetFunction::m_cols() const {
    return m_cols_;
}

double TargetFunction::ax_left_bound() const {
    return ax_left_bound_;
}

double TargetFunction::bx_right_bound() const {
    return bx_right_bound_;
}

double TargetFunction::ay_left_bound() const {
    return ay_left_bound_;
}

double TargetFunction::by_right_bound() const {
    return by_right_bound_;
}

double TargetFunction::hx() const {
    return (bx_right_bound() - ax_left_bound())/(n_rows() - 1);
}

double TargetFunction::hy() const {
    return (by_right_bound() - ay_left_bound())/(m_cols() - 1);
}

void TargetFunction::print_function_matrix(bool flag_skip) const {

    if (!flag_skip) return;

    cout << "Function values on mesh:" << endl;
    for (int i = 0; i < n_rows(); ++i) {
        for (int j = 0; j < m_cols(); ++j) {
            cout << std::setw(12) << value(i,j);
        }
        cout << endl;
    }
}

void TargetFunction::print_initial_info() const {

    cout << " Function Generator: " << endl;
    cout << " Domain is a [" << ax_left_bound() << ",";
    cout << bx_right_bound() << "] x [" << ay_left_bound() << ",";
    cout << by_right_bound() << "] rectangle" << endl;
    cout << " with n_x = " << n_rows() << ", m_y = " << m_cols() << " points." << endl;
}

///////////////////////////////////////////////////////////////////////////////

// End of the file

