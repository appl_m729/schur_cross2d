#ifndef _ARRAY_HOLDER_H_
#define _ARRAY_HOLDER_H_

#include <iostream>
#include "uncopyable.h"
#include "cblas.h"

using  std::cout;
using  std::endl;
using  std::setw;

///////////////////////////////////////////////////////////////////////////////

/*! The class holds memory pointers to allocated arrays
 *  (double, int and others). When the reallocation occures,
 *  a copy of old array to new memory can be done.
 *
 *  *** This module is successfully applicable for the
 *  row-like array format with a given capacity.
 *
 *  Version  1.2.0.   05-12-2014,
 *  Version  1.3.0    09-12-2014.
 *
 */

template<class T>
class BaseArrayHolder : public Uncopyable {
public:

    BaseArrayHolder():
        Uncopyable(),
        num_rows_(1),
        col_size_(0),
        capacity_(4),
        A_(new T[num_rows_ * capacity_]) {
    }

    virtual ~BaseArrayHolder() {
        delete[] A_;  A_ = NULL;
    }

    // Number of rows is fixed during all the reallocation.
    void set_fixed_rows_size(const int nrows) {
        if (nrows < 1) {
            cout << "Error in BaseArrayHolder! Wrong rows number" << endl;
            return;
        }
        reallocate_row_memory_(nrows);
    }

    // Number of columns is increased.
    virtual bool update(const int new_col_size) = 0;

    T * const adress()   {return A_;}
    int num_rows() const {return num_rows_;}
    int col_size() const {return col_size_;}
    int capacity() const {return capacity_;}
    double& v(const int i, const int j) {
        return A_[i * capacity_ + j];
    }

protected:

    int num_rows_;
    int col_size_;
    int capacity_;  // Capacity means 1d number of columns. Memory is
    T * A_;         // allocated as a 2d array of size num_rows_ x capacity_

    void  capacity_resize_(const int new_col_size) {
        // "Heuristic" determination of new capacity.
        if (new_col_size > (2 * this->capacity_)) {
            // The new size is supposed to be very big.
            this->capacity_ += new_col_size;
        } else {
            // The new size does not increase 2 x capacity_.
            this->capacity_ = 2 * new_col_size;
        }
    }

    virtual void  reallocate_row_memory_(const int new_rows) = 0;
};

///////////////////////////////////////////////////////////////////////////////

class DoubleCopyBoxArrayHolder: public BaseArrayHolder<double> {
public:

    DoubleCopyBoxArrayHolder() : BaseArrayHolder<double>() {
        // Reallocates memory allocated by default.
        // Parent constructor allocates only linear size array.
        // Here 2D-size array is needed.
        update(capacity_ + 1);  // now A_ is 2D of size capacity_ x capacity_.
    }
    virtual ~DoubleCopyBoxArrayHolder() {}

    virtual bool update(const int new_col_size) {

        if (this->capacity_ < new_col_size) {

            const int old_capacity = this->capacity_;
            this->capacity_resize_(new_col_size);

            // Now this->capacity_ is of new size.
            double * const buffer = new double[this->capacity_ * this->capacity_];
            // Copy old data block to the new array.
            for (int i = 0; i < this->col_size_; ++i) {
                cblas_dcopy(this->col_size_, &(this->A_[i * old_capacity]), 1,
                                             &buffer[i * this->capacity_],  1);
            }
            // Free old array. Swich A_ to buffer.
            delete[] this->A_;
            this->A_ = buffer;

            if (NULL == this->A_) {
                cout << "DoubleCopyBoxArrayHolder Error: Can't allocate memory\n";
                return false;
            }
        }

        this->col_size_ = new_col_size;
        this->num_rows_ = new_col_size;

        return true;
    }

private:

    virtual void  reallocate_row_memory_(const int new_rows) {
        this->update(new_rows);
    }
};

///////////////////////////////////////////////////////////////////////////////

template<class T>
class RawArrayHolder : public BaseArrayHolder<T> {
public:
    RawArrayHolder(): BaseArrayHolder<T>() {}
    virtual ~RawArrayHolder() {}

    virtual bool update(const int new_col_size) {

        if (this->capacity_ < new_col_size) {

            this->capacity_resize_(new_col_size);

            // Reallocation without copy. Delete old array, allocate a new one.
            delete[] this->A_; this->A_ = NULL;
            this->A_ = new T[this->capacity_ * this->num_rows_];

            if (NULL == this->A_) {
                cout << "RawArrayHolder Error: Can't allocate memory\n";
                return false;
            }
        }

        this->col_size_ = new_col_size;
        return true;
    }

private:

    virtual void reallocate_row_memory_(const int nrow) {

        if (nrow > this->num_rows_) {
            delete[] this->A_;  this->A_ = NULL;
            this->A_ = new T[nrow * this->capacity_];
        }
        this->num_rows_ = nrow;
    }
};

///////////////////////////////////////////////////////////////////////////////

template<class T>
class CopyArrayHolder : public BaseArrayHolder<T> {
public:
    CopyArrayHolder(): BaseArrayHolder<T>() {}
    virtual ~CopyArrayHolder() {}

    virtual bool update(const int new_col_size) {

        if (this->capacity_ < new_col_size) {

            const int old_capacity = this->capacity_;
            this->capacity_resize_(new_col_size);

            // Copy from old array to the new memory.
            T * const buffer = new T[this->capacity_ * this->num_rows_];
            // For int type it uses direct "cycle for"-copy.
            // For double type it calls BLAS routine
            pure_copy_(buffer, old_capacity);

            // Free old array. Swich A_ to buffer.
            delete[] this->A_;
            this->A_ = buffer;

            if (NULL == this->A_) {
                cout << "CopyArrayHolder Error: Can't allocate memory\n";
                return false;
            }
        }

        this->col_size_ = new_col_size;
        return true;
    }

protected:

    virtual void pure_copy_(T * const buff, const int oldCapacity) {

        // this->col_size_ is "old" here.
        for (int i = 0; i < this->num_rows_; ++i) {
            for (int k = 0; k < this->col_size_; ++k) {
                buff[i * this->capacity_ + k] = this->A_[i * oldCapacity + k];
            }
        }
    }

    virtual void reallocate_row_memory_(const int nrow) {

        if (nrow > this->num_rows_) {

            // Copy from old array to the new memory.
            T * const buffer = new T[nrow * this->capacity_];
            // For int type it uses direct "cycle for"-copy.
            // For double type it calls BLAS routine
            pure_copy_(buffer, this->capacity_);

            // Free old array. Swich A_ to buffer.
            delete[] this->A_;
            this->A_ = buffer;
        }
        this->num_rows_ = nrow;
    }
};

///////////////////////////////////////////////////////////////////////////////

class DoubleCopyArrayHolder : public CopyArrayHolder<double> {
public:
    DoubleCopyArrayHolder(): CopyArrayHolder<double>() {}
    virtual ~DoubleCopyArrayHolder() {}

protected:
    virtual void pure_copy_(double * const buff, const int oldC) {

        // this->col_size_ is "old" here.
        for (int i = 0; i < this->num_rows_; ++i) {
            cblas_dcopy(this->col_size_, &(this->A_[i * oldC]), 1, &buff[i * this->capacity_], 1);
        }
    }
};

///////////////////////////////////////////////////////////////////////////////

template <typename T>
void print_matrix(BaseArrayHolder<T> * A, const char str1[]) {

    cout << str1 << endl;
    for (int i = 0; i < A->num_rows(); ++i) {
        for (int j = 0; j < A->col_size(); ++j) {
            cout << setw(12) << A->adress()[i * A->capacity() + j];
        }
        cout << endl;
    }
}

///////////////////////////////////////////////////////////////////////////////

#endif  // _ARRAY_HOLDER_H_

// End of the file

