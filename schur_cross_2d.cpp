#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <algorithm>
#include "cblas.h"
#include "lapacke.h"
#include "schur_cross_2d.h"
#include "maxvol_engine.h"
#include "target_function.h"
#include "support_tools.h"
#include "matrix_tools.h"
#include "matrix_tools.h"
#include "cmath"

using std::cout;
using std::endl;
using std::setw;

///////////////////////////////////////////////////////////////////////////////

SchurCross2D::SchurCross2D():
t_start_(clock()),
tfunc_(0),
mxvl_engine_(new MaxvolEngine()),
aahatSlvr_(new AAhatSolver()),
clmnUpd_(new ColumnUpdater()),
submApnd_(new SubmatrixAppend()),
indApnd_(new IndexAppend()),
uv_approx_(new ProximityEstimator()),
rank_increment_flag_(false),
rank_truncation_flag_(false),
iter_limit_(2014),
rank_inc_(3),
eps_rank_(1e-6),
rank_(0),
UUhat_(new DoubleCopyArrayHolder()),
VVhat_(new DoubleCopyArrayHolder()),
u_sub_(new DoubleCopyArrayHolder()),
v_sub_(new DoubleCopyArrayHolder()),
Abox_(new DoubleCopyBoxArrayHolder()),
buff_1_(new RawArrayHolder<double>()),
buff_2_(new RawArrayHolder<double>()),
buff_3_(new RawArrayHolder<double>()),
Ur_(NULL),
Vr_(NULL),
row_index_(new CopyArrayHolder<int>()),
col_index_(new CopyArrayHolder<int>()),
row_inc_index_(new CopyArrayHolder<int>()),
col_inc_index_(new CopyArrayHolder<int>()),
I_buff_(new RawArrayHolder<int>()) {

    // Default values for maxvol.
    mxvl_engine_->set_tolerance(1e-2);
    mxvl_engine_->set_iter_limit(2014);

    // ColumnUpdater uses maxvol search.
    clmnUpd_->set_maxvol_solver(mxvl_engine_);

    // ProximityEstimator uses pointers of constant arrays.
    uv_approx_->set_UUhat(UUhat_);
    uv_approx_->set_VVhat(VVhat_);
    uv_approx_->set_u_sub(u_sub_);
    uv_approx_->set_v_sub(v_sub_);
    uv_approx_->set_Abox(Abox_);
    uv_approx_->set_buffers(buff_1_, buff_2_, buff_3_);
    uv_approx_->set_row_index(row_index_);
    uv_approx_->set_row_inc_index(row_inc_index_);
    uv_approx_->set_col_index(col_index_);
    uv_approx_->set_col_inc_index(col_inc_index_);
}

SchurCross2D::~SchurCross2D() {

    delete  I_buff_;
    delete  col_inc_index_;
    delete  row_inc_index_;
    delete  col_index_;
    delete  row_index_;
    delete  buff_3_;
    delete  buff_2_;
    delete  buff_1_;
    delete  Abox_;
    delete  v_sub_;
    delete  u_sub_;
    delete  VVhat_;
    delete  UUhat_;
    delete  uv_approx_;
    delete  indApnd_;
    delete  submApnd_;
    delete  clmnUpd_;
    delete  aahatSlvr_;
    delete  mxvl_engine_;

    Ur_ = NULL;
    Vr_ = NULL;
}

bool SchurCross2D::set_target_function(TargetFunction * const targetFunc) {

    if (targetFunc) {
        tfunc_ = targetFunc;
        return true;
    }
    return false;
}

bool SchurCross2D::set_rank_increment(const int RankInc) {

    if (RankInc > 0 && RankInc < 1001) {
        rank_inc_ = RankInc;
        uv_approx_->set_inc_rank(RankInc);
        return (rank_increment_flag_ = true);
    }
    return (rank_increment_flag_ = false);
}

// Singular values less then epsilon are thrown out.
bool SchurCross2D::set_rank_truncation(const double epsilon) {

    if (epsilon > 0) {
        eps_rank_ = epsilon;
        return (rank_truncation_flag_ = true);
    }
    return (rank_truncation_flag_ = false);
}

bool SchurCross2D::set_iter_limit(const int max_iter) {

    if (max_iter > 0) {
        iter_limit_ = max_iter;
        return true;
    }
    return false;
}

// Sets up the tolerance of the maxvol subroutine.
bool SchurCross2D::set_maxvol_tolerance(const double tolerance_maxvol) {
    return mxvl_engine_->set_tolerance(tolerance_maxvol);
}

bool SchurCross2D::set_maxvol_iter_limit(const int max_iter) {
    return mxvl_engine_->set_iter_limit(max_iter);
}

// Call this routine before calling find_approximate_decomposition()
bool SchurCross2D::are_all_params_setup_properly() const {

    if (!rank_increment_flag_) {
        cout << "SchurCross2D: Rank increment wrong setup." << endl;
    }

    if (!rank_truncation_flag_) {
        cout << "SchurCross2D: Rank truncation wrong setup." << endl;
    }

    const bool func_ok = static_cast<bool>(tfunc_);
    if (!func_ok) {
        cout << "SchurCross2D: Target function is not set." << endl;
    }

    if (rank_inc_ > tfunc_->n_rows()) return false;
    if (rank_inc_ > tfunc_->m_cols()) return false;

    return (rank_increment_flag_ && func_ok && rank_truncation_flag_);
}

bool SchurCross2D::find_approximate_decomposition() {

    // Reset random seed.
    srand(time(NULL));

    if (!are_all_params_setup_properly()) {return false;}

    // 1.0 Update memory capacity for all int and double matrices.
    update_memory_(rank_inc_);

    // 1.0.1 Initial rank value.
    rank_ = rank_inc_;
    uv_approx_->update_rank(rank_);

    // 1.0.2 Initialization.
    Ur_ = buff_1_;
    Ur_->set_fixed_rows_size(tfunc_->n_rows());  // Memory is already allocated.
    Vr_ = buff_2_;
    Vr_->set_fixed_rows_size(tfunc_->m_cols());  // Memory is already allocated.

    // 1.1 Random filling.
    random_init_(Ur_);
    random_init_(Vr_);

    // 1.2 Orthogonalization. Matrix Q is in A. Buffer is of size of A.
    QR_of_A_(Ur_);  // buff_3_ is used (busy) here.
    QR_of_A_(Vr_);  // buff_3_ is used (busy) here.

    // 1.3 Maxvol search. None buffer is used.
    find_maxvol_indexes_(Ur_, row_index_);
    find_maxvol_indexes_(Vr_, col_index_);

    // 1.4 Setup maxvol submatrix.
    calc_cols_submatrix_(col_index_->adress());
    calc_rows_submatrix_(row_index_->adress());

    // 1.5 Initial setup of Abox. Its size is rank_inc_ x rank_inc_.
    init_Abox_();    // copy from u_sub_

    // 1.6 Solve
    solve_AAhat_(Ur_, UUhat_, row_index_);  // buff_3_ is used here.
    solve_AAhat_(Vr_, VVhat_, col_index_);  // buff_3_ is used here.

    Ur_ = Vr_ = NULL;  // Clean pointers.

    // 2. Main loop.
    for (int k_iter = 0; k_iter < iter_limit_; ++k_iter) {

        cout << "k_iter = " << k_iter << "  rank = " << rank_ << endl;

        // 2.1 Check rank vs initial_matrix_size condition.
        if (!check_size_rank_condition_()) {
            return false;
        }

        // 2.2 Before adding columns check memory allocation.
        update_memory_(rank_ + rank_inc_);

        // 2.3 Appennd S x S^{-1} to U x U_hat. S = Q x R
        // bool
        bool ok1 = column_update_(UUhat_, u_sub_, row_index_, row_inc_index_);
        bool ok2 = column_update_(VVhat_, v_sub_, col_index_, col_inc_index_);
        if (!ok1 || !ok2) {break;}

        // 2.5 Concatinate maxvol Index with new Ind_sub.
        ok1 = append_inc_Index_(row_index_, row_inc_index_);
        ok2 = append_inc_Index_(col_index_, col_inc_index_);
        if (!ok1 || !ok2) {break;}

        // 2.6 Update rank value.
        rank_ += rank_inc_;
        uv_approx_->update_rank(rank_);

        // 2.7
        calc_cols_submatrix_(col_inc_index_->adress());
        calc_rows_submatrix_(row_inc_index_->adress());

        // 2.8 Append new "good" rows and columns.
        update_Abox_();

        // 2.9 Comparision condition. buff_1_ is used here.
        if (is_approximation_quite_good_(k_iter)) {
            return true;
        }
    }

    cout << "Warning! Schur-Cross-2d did not converge."<< endl;
    return false;
}

double SchurCross2D::approx_value(const int i, const int j) {

    double * const pU = Ur_->adress();
    const int ldu = Ur_->capacity();

    double * const pV = Vr_->adress();
    const int ldv = Vr_->capacity();

    return cblas_ddot(rank_, &pU[i * ldu], 1, &pV[j * ldv], 1);
}

const double * SchurCross2D::U_adress() const {
    return Ur_->adress();
}

int SchurCross2D::ld_U() const {
    return Ur_->capacity();
}

const double * SchurCross2D::V_adress() const {
    return Vr_->adress();
}

int SchurCross2D::ld_V() const {
    return Vr_->capacity();
}

bool SchurCross2D::check_size_rank_condition_() {

    const int n = tfunc_->n_rows();
    const int m = tfunc_->m_cols();
    const int next_rank = rank_ + rank_inc_;

    if (next_rank > n || next_rank > m) {

        cout << "SchurCross2D: Error for size condition:";
        cout << " rank exceeds one of the matrix sizes!" << endl;
        cout << "upcoming rank = " << next_rank << endl;
        cout << "rows number = " << n << endl;
        cout << "cols number = " << m << endl;

        return false;
    }
    return true;
}

bool SchurCross2D::update_memory_(const int newRank) {

    if (newRank <= rank_) {
        cout << "SchurCross2D Error! Update rank to decrimental size." << endl;
        return false;
    }

    // Matrices with data. Columns of full length n.
    const int n = tfunc_->n_rows();
    // Increase columns-array capacity.
    UUhat_->set_fixed_rows_size(n);
    UUhat_->update(newRank);
    u_sub_->set_fixed_rows_size(n);
    u_sub_->update(rank_inc_);

    // Intersection of "good" rows and columns.
    Abox_->update(newRank);

    // Matrices with data. Rows of full size m.
    const int m = tfunc_->m_cols();
    // Increase rows-array capacity.
    VVhat_->set_fixed_rows_size(m);
    VVhat_->update(newRank);
    v_sub_->set_fixed_rows_size(m);
    v_sub_->update(rank_inc_);

    // Buffers (double type)
    const int max_nm = std::max<int>(n, m);
    buff_1_->set_fixed_rows_size(max_nm);
    buff_1_->update(newRank);
    buff_2_->set_fixed_rows_size(max_nm);
    buff_2_->update(newRank);
    buff_3_->set_fixed_rows_size(max_nm);
    buff_3_->update(newRank);

    // Indexes
    row_index_->set_fixed_rows_size(1);
    row_index_->update(newRank);
    col_index_->set_fixed_rows_size(1);
    col_index_->update(newRank);
    row_inc_index_->set_fixed_rows_size(1);
    row_inc_index_->update(rank_inc_);
    col_inc_index_->set_fixed_rows_size(1);
    col_inc_index_->update(rank_inc_);

    // Buffer Index (int type)
    I_buff_->update(max_nm);
}

bool SchurCross2D::random_init_(BaseArrayHolder<double> * const U) {

    double * const pU = U->adress();
    const int cap = U->capacity();

    for (int i = 0; i < U->num_rows(); ++i) {
        for (int j = 0; j < U->col_size(); ++j) {
            const double rng01 = static_cast<double>(rand()) / static_cast<double>(RAND_MAX - 1);
            const double dvalue = 2.0 * rng01 - 1.0;
            pU[i * cap + j] = dvalue;
        }
    }
    return true;
}

bool SchurCross2D::QR_of_A_(BaseArrayHolder<double> * const U) {

    // Just temporary variables for one-shot usage.
    const int n = U->num_rows();
    const int r = U->col_size();
    const int ldu = U->capacity();
    double * const pU  = U->adress();
    double * const tau = buff_3_->adress();

    // 1. QR via Hausholder. Especial storage format.
    int ok = LAPACKE_dgeqrf(LAPACK_ROW_MAJOR, n, r, pU, ldu, tau);
    if (ok) {
        cout << "SchurCross2D: QR1 error! code = " << ok << endl;
        return false;
    }

    // 2. Restore Q from lapack format.
    ok = LAPACKE_dorgqr(LAPACK_ROW_MAJOR, n, r, r, pU, ldu, tau);
    if (ok) {
        cout << "SchurCross2D: QR2 error! code = " << ok << endl;
        return false;
    }

    return true;
}

bool SchurCross2D::find_maxvol_indexes_(BaseArrayHolder<double> * const U,
                                        BaseArrayHolder<int> * const Index) {

    mxvl_engine_->set_rows_long_size(U->num_rows());
    mxvl_engine_->set_columns_short_size(U->col_size());
    mxvl_engine_->set_columns_capacity(U->capacity());
    mxvl_engine_->set_matrix_pointer(U->adress());

    if (!mxvl_engine_->are_all_params_setup_properly()) {return false;}

    return mxvl_engine_->find_maximum_volume(Index->adress());
}

void SchurCross2D::calc_cols_submatrix_(const int * const col_Index) {

    const int n = u_sub_->num_rows();
    const int r = u_sub_->col_size();
    const int ldsub = u_sub_->capacity();
    double * const pu_sub = u_sub_->adress();

    for (int j = 0; j < r; ++j) {
        const int Ind_j = col_Index[j];
        for (int i = 0; i < n; ++i) {
            pu_sub[i * ldsub + j] = tfunc_->value(i, Ind_j);
        }
    }
}

void SchurCross2D::calc_rows_submatrix_(const int * const row_Index) {

    const int r = v_sub_->col_size();
    const int m = v_sub_->num_rows();
    const int ldsub = v_sub_->capacity();
    double * const pv_sub = v_sub_->adress();

    for (int i = 0; i < r; ++i) {
        const int Ind_i = row_Index[i];
        for (int j = 0; j < m; ++j) {
            pv_sub[j * ldsub + i] = tfunc_->value(Ind_i, j);
        }
    }
}

// Wrapper for U and V solvers.
bool SchurCross2D::solve_AAhat_(BaseArrayHolder<double> * const U,
                                BaseArrayHolder<double> * const UUhat,
                                BaseArrayHolder<int> * const Index) {

    aahatSlvr_->set_A_pointer(U->adress());
    aahatSlvr_->set_rows_long_size(U->num_rows());
    aahatSlvr_->set_cols_short_size(U->col_size());
    aahatSlvr_->set_cols_capacity(U->capacity());

    aahatSlvr_->set_maxvol_Index(Index->adress());

    aahatSlvr_->set_matrix_buffer(buff_3_->adress());
    aahatSlvr_->set_Index_buffer(I_buff_->adress());

    return aahatSlvr_->update(UUhat->adress(), UUhat->capacity());
}

bool SchurCross2D::column_update_(BaseArrayHolder<double> * const UUhat,
                                  BaseArrayHolder<double> * const u_sub,
                                  BaseArrayHolder<int> * const U_Index,
                                  BaseArrayHolder<int> * const inc_Ind) {

    clmnUpd_->set_a_sub(u_sub->adress());    // u_sub is not changed
    clmnUpd_->set_Index(U_Index->adress());  // index is not changed

    clmnUpd_->set_buffers(buff_1_->adress(), buff_2_->adress(), buff_3_->adress());
    clmnUpd_->set_Ibuffer(I_buff_->adress());

    clmnUpd_->set_maxvol_solver(mxvl_engine_);

    clmnUpd_->set_rows_long_size(UUhat->num_rows());
    clmnUpd_->set_A_cols_short_size(rank_);  // old size is needed here
    clmnUpd_->set_A_cols_capacity(UUhat->capacity());

    clmnUpd_->set_a_sub_cols_short_size(rank_inc_);
    clmnUpd_->set_a_sub_cols_capacity(u_sub->capacity());

    // Updated and increased AA and new index_add.
    return clmnUpd_->update(UUhat->adress(), inc_Ind->adress());
}

bool SchurCross2D::append_asub_to_A_(BaseArrayHolder<double> * const AAhat,
                                     BaseArrayHolder<double> * const a_sub) {

    submApnd_->set_rows_long_size(AAhat->num_rows());
    submApnd_->set_A_cols_short_size(rank_);
    submApnd_->set_A_cols_capacty(AAhat->capacity());

    submApnd_->set_asub(a_sub->adress());
    submApnd_->set_asub_cols_short_size(rank_inc_);
    submApnd_->set_asub_cols_capacity(a_sub->capacity());

    return submApnd_->update(AAhat->adress());
}

bool SchurCross2D::append_inc_Index_(BaseArrayHolder<int> * const Index,
                                     BaseArrayHolder<int> * const incInd) {

    indApnd_->set_add_Index(incInd->adress());
    indApnd_->set_add_size(rank_inc_);

    return indApnd_->update(Index->adress(), rank_);
}

void SchurCross2D::init_Abox_() {

    Abox_->update(rank_inc_);

    double * const pAbox = Abox_->adress();
    const int ldab = Abox_->capacity();

    double * const pusub = u_sub_->adress();
    const int ldsub = u_sub_->capacity();

    for (int i = 0; i < rank_inc_; ++i) {

        const int ind_i = row_index_->adress()[i];
        for (int j = 0; j < rank_inc_; ++j) {

            pAbox[i * ldab + j] = pusub[ind_i * ldsub + j];
        }
    }
}

bool SchurCross2D::update_Abox_() {

    double * const pAbox = Abox_->adress();
    const int ldab       = Abox_->capacity();
    double * const puSub = u_sub_->adress();
    const int lduSub     = u_sub_->capacity();
    double * const pvSub = v_sub_->adress();
    const int ldvSub     = v_sub_->capacity();
    const int drank      = rank_ - rank_inc_;        // decremented rank;

    if (drank < 0) {
        cout << "Error in update_Abox: decremented rank is negative!" << endl;
        return false;
    }

    // 1. Copy full u_sub_ to Abox columns.
    for (int i = 0; i < rank_; ++i) {
        const int ind_i = row_index_->adress()[i];
        cblas_dcopy(rank_inc_, &puSub[ind_i * lduSub], 1, &pAbox[i * ldab + drank], 1);
    }

    // 2. Copy informative v_sub_ to Abox rows.
    for (int j = 0; j < drank; ++j) {
        const int ind_j = col_index_->adress()[j];
        cblas_dcopy(rank_inc_, &pvSub[ind_j * ldvSub], 1, &pAbox[drank * ldab + j], ldab);
    }

    return true;
}

bool SchurCross2D::is_approximation_quite_good_(const int k_iter) {

    if (uv_approx_->is_approximation_quite_good(eps_rank_)) {

        // 3. Final rounding. QR for U and V, then SVD for Ru * Ar * RvT
        QR_orthogonalization_with_SVD_rounding_();

        cout << "SchurCross2D: Approximation converged with rank = ";
        cout << rank_ << endl;
        cout << "SchurCross2D: number of iterations = " << (k_iter + 1) << endl;

        const clock_t t_stop = clock();
        const clock_t t_diff = t_stop - t_start_;
        cout << "SchurCross2D: decomposition takes " << t_diff << " clocks, i.e. ";
        cout << std::setprecision(3) << static_cast<double>(t_diff)/CLOCKS_PER_SEC << " seconds " << endl;

        return true;
    }
    return false;
}

void SchurCross2D::QR_orthogonalization_with_SVD_rounding_() {

    // 1.1. UUhat
    BaseArrayHolder<double> * const Ru = buff_1_;
    final_QR_of_AAhat_(UUhat_, Ru);     // buff_3_ is used here for tau array

    // 1.2. VVhat
    BaseArrayHolder<double> * const Rv = buff_2_;
    final_QR_of_AAhat_(VVhat_, Rv);     // buff_3_ is used here for tau array

    // 3. Make matrix G = Ru * Abox * Rv^T
    double * const pG = buff_1_->adress();  // will be used after Ru is free.
    calc_Ru_Abox_RvT_product_(Ru, Rv, pG);  // buff_3_ is used here.

    // 4. SVD decomposition and rounding.
    //    buff_1_ and buff_2_ are available now.
    double * const Usvd  = buff_1_->adress();
    double * const Vsvd  = buff_2_->adress();
    double * const Ssvd  = Abox_->adress();   // Now can be used as a buffer.
    double * const dWork = buff_3_->adress(); //u_sub_->adress(); // Now it is used as a buffer.
    int size = Ru->col_size();

    // Truncated rank.
    const int r_svd = SVD_round_of_(pG, size, Usvd, Ssvd, Vsvd, dWork);

    // 5.1 Multiplication Qu * Ru after SVD truncation.
    Ur_ = buff_3_;
    Ur_->set_fixed_rows_size(tfunc_->n_rows());
    Ur_->update(r_svd);   // Reset number of columns = svd-rank
    mult_Qa_Ra_(Ur_, UUhat_, Usvd, CblasNoTrans);

    // 5.2 Multiplication Qv * Rv after SVD truncation.
    Vr_ = UUhat_;
    Vr_->set_fixed_rows_size(tfunc_->m_cols());
    Vr_->update(r_svd);
    mult_Qa_Ra_(Vr_, VVhat_, Vsvd, CblasTrans);

    // 6.0 Rank truncation setup.
    rank_ = r_svd;

    // 7.1 Push sqrt(Sigma) in U: Ur * sqrt(sigma).
    push_sqrt_sigma_in_(Ur_, Ssvd);

    // 7.2 Push sqrt(Sigma) in V: Vr * sqrt(sigma).
    //     Is saved in the not transposed format.
    push_sqrt_sigma_in_(Vr_, Ssvd);
}

// QR decomposition of AAhat with R returned.
bool SchurCross2D::final_QR_of_AAhat_(BaseArrayHolder<double> * const AAhat,
                                      BaseArrayHolder<double> * const its_R) {
    // Just temporary usage.
    const int n   = AAhat->num_rows();
    const int r   = AAhat->col_size();
    const int lda = AAhat->capacity();
    double * const pAAhat  = AAhat->adress();
    double * const tau = buff_3_->adress();
    double * const Ra = its_R->adress();

    // 1. QR via Hausholder. Especial storage format.
    //    Matrix R is in right-hanf side.
    int ok = LAPACKE_dgeqrf(LAPACK_ROW_MAJOR, n, r, pAAhat, lda, tau);
    if (ok) {
        cout << "SchurCross2D: final_QR_of_AAhat_ error1! code = " << ok << endl;
        return false;
    }

    // 2. Copy R matrix from lapack storage format of qrf to a separate buffer.
    //    Serial copy of every column.
    for (int i = 0; i < r; ++i) {

        const int x_length = i + 1;
        const int o_length = r - x_length;

        // 2.1. Copy valuable data for Ru.
        cblas_dcopy(x_length, &pAAhat[i], lda, &Ra[i], r);

        // 2.2. Zero initialization of the rest (zero) part of matrix Ru.
        if (o_length) {
            cblas_dscal(o_length, 0, &Ra[(i + 1) * r + i], r);
        }
    }

    // 3. Restore Q from lapack format.
    ok = LAPACKE_dorgqr(LAPACK_ROW_MAJOR, n, r, r, pAAhat, lda, tau);
    if (ok) {
        cout << "SchurCross2D: final_QR_of_AAhat_ error2! code = " << ok << endl;
        return false;
    }
    return true;
}

// Ru is in buff_1_
// Rv is in buff_2_
bool SchurCross2D::calc_Ru_Abox_RvT_product_(BaseArrayHolder<double> * const Ru,
                                             BaseArrayHolder<double> * const Rv,
                                             double * const G_prod) {
    // 1. Ru * Abox
    double * const tmp_Ru_Abox = buff_3_->adress();  // Buffer is free.
    const int r = Ru->col_size();
    //    Abox auxiliary data.
    double * const pAbox = Abox_->adress();
    const int lda = Abox_->capacity();

    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, r, r, r, 1.0,
                Ru->adress(), r, pAbox, lda, 0.0, tmp_Ru_Abox, r);
    //          buff1_                            buff3_

    // 2. (Ru * Abox) * Rv^T.  buff1_ can be used now.
    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, r, r, r, 1.0,
                tmp_Ru_Abox, r, Rv->adress(), r, 0.0, G_prod, r);
    //          buff3_          buff2_                buff1_

    return true;
}

int SchurCross2D::SVD_round_of_(double * const G, const int r,
                                double * const U, double * const S,
                                double * const V, double * buffer) {
    // 1. SVD decomposition.
    const bool ok = LAPACKE_dgesvd(LAPACK_ROW_MAJOR, 'A', 'A', r, r, G, r,
                                                   S, U, r, V, r, buffer);
    if (ok) {
        cout << "SchurCross2D: SVD rounding error! code = " << ok << endl;
        return 0;
    }

    // 2. Rounding.
    return rounded_rank_(r, S, buffer);
}

int SchurCross2D::rounded_rank_(const int full_rank,  double * S,
                                double * const buff) {
    // 1.  S / S[0]
    // 1.1 Check first singular value against zero.
    if (fabs(S[0]) < 1e-300) {   // Yes, it is always positive.
        cout << "Error in rounding of rank. Max singular value = ";
        cout << S[0] << endl;
        return 1;
    }

    // 1.2  Copy Singulars to buffer.
    cblas_dcopy(full_rank, S, 1, buff, 1);
    S = buff;   // just convenient reference

    // 1.3  Divide Singular values vector by S[0].
    const double alpha = static_cast<double>(1.0) / S[0];
    cblas_dscal(full_rank, alpha, S, 1);

    // 2.0  Compute full norm.
    const double full_nrm = cblas_dnrm2(full_rank, S, 1);

    // 3.0  Compute tail relation and get rank from it.
    double a = 0;
    for (int Kr = full_rank - 1; Kr >= 0; --Kr) {

        const double max_ab = std::max<double>(fabs(S[Kr]), a);
        if (max_ab < 1e-300) {
            // Just running to at least last non-zero singular.
            continue;
        }

        const double eta_1 = S[Kr] / max_ab;
        const double eta_2 = a / max_ab;

        a = max_ab * sqrt(eta_1 * eta_1 + eta_2 * eta_2);

        if (a > full_nrm * eps_rank_) {
            return Kr + 1;
        }
    }

    // Totally degenerate matrix with rank 1.
    return 1;
}

bool SchurCross2D::mult_Qa_Ra_(BaseArrayHolder<double> * const Ur,
                               BaseArrayHolder<double> * const UUhat,
                               double * const Usvd,
                               const enum CBLAS_TRANSPOSE TransA) {

    double * const pUr = Ur->adress();
    const int ldUr  = Ur->capacity();
    const int r_svd = Ur->col_size();  // Must be updated before call this routine.

    double * const pU_Q = UUhat->adress();
    const int n    = UUhat->num_rows();
    const int lduq = UUhat->capacity();

    cblas_dgemm(CblasRowMajor, CblasNoTrans, TransA, n, r_svd, rank_,
                       1.0, pU_Q, lduq, Usvd, rank_, 0.0, pUr, ldUr);
}

void SchurCross2D::push_sqrt_sigma_in_(BaseArrayHolder<double> * const Ar,
                                                       double  * const Sigma) {
    double * const pA = Ar->adress();
    const int lda = Ar->capacity();
    const int n = Ar->num_rows();

    for (int k = 0; k < rank_; ++k) {
        const double sigma_k = sqrt(Sigma[k]);
        cblas_dscal(n, sigma_k, &pA[k], lda);
    }
}

///////////////////////////////////////////////////////////////////////////////

// End of the file

