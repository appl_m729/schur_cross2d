#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <algorithm>
#include <cmath>
#include "cblas.h"
#include "lapacke.h"
#include "maxvol_engine.h"
#include "support_tools.h"

using std::cout;
using std::endl;
using std::setw;

///////////////////////////////////////////////////////////////////////////////

MaxvolEngine::MaxvolEngine():
Uncopyable(),
flag_n_row_(false),
flag_r_col_(false),
flag_p_mtrx_(false),
eps_tol_(1e-2),
n_row_(0),
r_col_(0),
capacity_(0),
max_iter_(1000),
pA_(NULL),
nr_buffer_size_(1000),
mtrx_data_(new double[nr_buffer_size_]),
mtrx_buff_(new double[nr_buffer_size_]),
ind_r_size_(100),
n_ind_size_(100),
Ip_(new int[ind_r_size_]),
buff_ind_(new int[n_ind_size_]) {

}

MaxvolEngine::~MaxvolEngine() {

    pA_ = NULL;
    delete[] buff_ind_;
    delete[] Ip_;
    delete[] mtrx_buff_;
    delete[] mtrx_data_;
}

bool MaxvolEngine::are_all_params_setup_properly() const {

    const bool nr_cond = (n_row_ >= r_col_) && (capacity_ >= r_col_);
    return (flag_n_row_ && flag_r_col_ && flag_p_mtrx_ && nr_cond);
}

bool MaxvolEngine::set_tolerance(const double Eps_tol) {

    if (Eps_tol > 1e-14) {
        eps_tol_ = Eps_tol;
        return true;
    }
    return false;
}

bool MaxvolEngine::set_rows_long_size(const int lnum_rows) {

    flag_n_row_ = false;
    if (lnum_rows > 1) {
        n_row_ = lnum_rows;
        flag_n_row_ = true;
        return true;
    }
    return false;
}

bool MaxvolEngine::set_columns_short_size(const int snum_cols) {

    flag_r_col_ = false;
    if (snum_cols > 1) {
        r_col_ = snum_cols;
        flag_r_col_ = true;
        return true;
    }
    return false;
}

bool MaxvolEngine::set_columns_capacity(const int in_Capacity) {

    if (in_Capacity > 1) {
        capacity_ = in_Capacity;
        return true;
    }
    return false;
}

bool MaxvolEngine::set_iter_limit(const int iter_num) {

    if (iter_num > 0) {
        max_iter_ = iter_num;
        return true;
    }
    return false;
}

bool MaxvolEngine::set_matrix_pointer(const double * const mtrx_pntr) {

    flag_p_mtrx_ = false;
    if (NULL != mtrx_pntr) {
        flag_p_mtrx_ = true;
        pA_ = mtrx_pntr;
        return true;
    }
    return false;
}

// The size of Index is r_col - smaller size.
bool MaxvolEngine::find_maximum_volume(int * const ind_box) {

    // Memory reallocation. All buffers.
    if (!is_all_reallocation_ok_()) {return false;}

    // Copy A for solving A = P * L * U  to buffer array mtrx_data_.
    copy_A_to_data_();

    // B = Transpose(A).
    transpose_A_to_Bt_();

    // Decompose A = P * L * U. L and U are stored in A(out)
    decompose_A_in_PLU_();

    // Ip_ -> ind_box :  (6456) -> (645132).
    // Ip_ is set here to unity permutation. Starts from 1.
    indexes_transform_(ind_box);       // buff_ind is used inside

    // Solves A_box^T * Y = A^T, Y = B^T, B = A * A_box^(-1)
    if (!solve_A_Abox_inverse_()) {return false;}

    // Buffer variables for B-rows update.
    // Just bind the poiters to unused allocated buffer.
    double * const u =  mtrx_data_;
    double * const v = &mtrx_data_[n_row_];

    // Main loop starts
    for (int k_iter = 0; k_iter < max_iter_; ++k_iter) {

        // Search position
        int big_ind_1d, ib, jb;
        const double bmax = get_local_argmax_(big_ind_1d, ib, jb);

        // Volume is "good enough". Main loop exit. **************
        if (fabs(bmax) <= 1.0 + eps_tol_)           {return true;}
        // Volume is "good enough". Main loop exit. **************

        // b[j0,:]
        cblas_dcopy(n_row_, &mtrx_buff_[jb * n_row_], 1, u, 1);

        // b[:,i0]
        cblas_dcopy(r_col_, &mtrx_buff_[ib], n_row_, v, 1);

        // b[:,i0] - b[:,I[j0]]
        cblas_daxpy(r_col_, -1.0, &mtrx_buff_[ind_box[jb]], n_row_, v, 1);

        // Bt = Bt - (b[:,i0] - b[:,Ip[j0]]) b[j0,:]/b[j0,i0]
        cblas_dger(CblasRowMajor, r_col_, n_row_, -1.0 / bmax, v, 1, u, 1,
                                                      mtrx_buff_, n_row_);
        // Save new "good" row index
        ind_box[jb] = ib;
    }

    cout << "Warning! Maxvol did not converge." << endl;
    return false;
}

bool MaxvolEngine::is_all_reallocation_ok_() {

    if (!is_1reallocation_ok_<int>(ind_r_size_, r_col_, Ip_, 2)) {
        cout << "Memory allocation: Ip_ failed" << endl;
        return false;
    }

    if (!is_1reallocation_ok_<int>(n_ind_size_, n_row_, buff_ind_, 2)) {
        cout << "Memory allocation: buff_ind_ failed" << endl;
        return false;
    }

    int nr_buffer_size_old = nr_buffer_size_;
    const int nr_size = n_row_ * r_col_;
    if (!is_1reallocation_ok_<double>(nr_buffer_size_, nr_size, mtrx_data_, 4)) {
        cout << "Memory allocation: mtrx_data_ failed" << endl;
        return false;
    }

    if (!is_1reallocation_ok_<double>(nr_buffer_size_old, nr_size, mtrx_buff_, 4)) {
        cout << "Memory allocation: mtrx_buff_ failed" << endl;
        return false;
    }

    return true;
}

void MaxvolEngine::copy_A_to_data_() {

    // Capacity is supposed to be setup already.
    for (int j = 0; j < r_col_; ++j) {
        cblas_dcopy(n_row_, &pA_[j], capacity_, &mtrx_data_[j], r_col_);
    }
}
// Old version of the code.
//    const int nr_size = n_row_ * r_col_;
//    cblas_dcopy(nr_size, pA_, 1, mtrx_data_, 1);

void MaxvolEngine::transpose_A_to_Bt_() {

    // B = Transpose(A)
    for (int k = 0; k < r_col_; ++k) {
        cblas_dcopy(n_row_, &mtrx_data_[k], r_col_, &mtrx_buff_[k * n_row_], 1);
    }
}

bool MaxvolEngine::decompose_A_in_PLU_() {

    const int err = LAPACKE_dgetrf(LAPACK_ROW_MAJOR, n_row_, r_col_, mtrx_data_, r_col_, Ip_);

    if (err) {
        cout << "Maxvol failed at LAPACKE_dgetrf(...)  error code = " << err << endl;
        return false;
    }
    return true;
}

void MaxvolEngine::indexes_transform_(int * const ind_box) {

    // C-like notation is used. Starts from 0,1,...
    // Unity permutation
    for (int i = 0; i < n_row_; ++i) {
        buff_ind_[i] = i;
    }

    // Convertation from LAPACK subroutines
    for (int i = 0; i < r_col_; ++i) {
        const int j = Ip_[i] - 1;
        if (i != j) {
            std::swap<int>(buff_ind_[i], buff_ind_[j]);
        }
    }

    // Fill proper indeces of maxvol submatrix.
    // Take first r_col_ rows from L * U (permuted A). Upper r_col_ rows compose A_box.
    for (int k = 0; k < r_col_; ++k) {
        ind_box[k] = buff_ind_[k];
        Ip_[k] = k + 1;
    }
}

bool MaxvolEngine::solve_A_Abox_inverse_() {

    // Solves A_box^T * Y = A^T, Y = B^T, B = A * A_box^(-1)
    const int err = LAPACKE_dgetrs(LAPACK_ROW_MAJOR, 'T', r_col_, n_row_, mtrx_data_,
                                                    r_col_, Ip_, mtrx_buff_, n_row_);
    if (err) {
        cout << "Maxvol failed at LAPACKE_dgetrs(...)\n";
        return false;
    }
    return true;
}

double MaxvolEngine::get_local_argmax_(int & big_ind_1d, int & ib, int & jb) {

    // 1d-index
    big_ind_1d = cblas_idamax(n_row_ * r_col_, mtrx_buff_, 1);

    // C-style indexing
    jb = big_ind_1d / n_row_;
    ib = big_ind_1d % n_row_;

    return mtrx_buff_[big_ind_1d];
}

///////////////////////////////////////////////////////////////////////////////

// End of the file

