#ifndef _UNCOPYABLE_BASE_H_
#define _UNCOPYABLE_BASE_H_

////////////////////////////////////////////////////////////////////////////

/*! \class Uncopyable
 *  \brief Class for protection of undesirable behaviour.
 *
 *  Modified  17.01.2013
 *
 *  The classes which inherits Uncopyable do not have
 *  Copy constructor and operator=().
 *  Such classes must not have their own copies.
 */

class Uncopyable {

protected:

    Uncopyable() {}
    ~Uncopyable() {}

private:

    Uncopyable(const Uncopyable&);
    Uncopyable& operator= (const Uncopyable&);
};

////////////////////////////////////////////////////////////////////////////

#endif // _UNCOPYABLE_BASE_H_

//end of file

