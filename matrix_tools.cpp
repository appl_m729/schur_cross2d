#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <set>
#include "cblas.h"
#include "lapacke.h"
#include "matrix_tools.h"
#include "maxvol_engine.h"

using  std::cout;
using  std::endl;
using  std::setw;
using  std::set;

///////////////////////////////////////////////////////////////////////////////

AAhatSolver::AAhatSolver():
Uncopyable(),
A_(0),
n_(0),
r_(0),
lda_(0),
Index_(0),
A_buff_(0),
I_buff_(0) {
}

AAhatSolver::~AAhatSolver() {
    A_      = 0;
    Index_  = 0;
    A_buff_ = 0;
    I_buff_ = 0;
}

void AAhatSolver::set_A_pointer(const double * const A) {
    A_ = A;
}

void AAhatSolver::set_rows_long_size(const int n_rows) {
    n_ = n_rows;
}

void AAhatSolver::set_cols_short_size(const int r_cols) {
    r_ = r_cols;
}

void AAhatSolver::set_cols_capacity(const int capacity) {
    lda_ = capacity;
}

void AAhatSolver::set_maxvol_Index(const int * const index) {
    Index_ = index;
}

void AAhatSolver::set_matrix_buffer(double * const a_buff) {
    A_buff_ = a_buff;
}

void AAhatSolver::set_Index_buffer(int * const Ind_buff) {
    I_buff_ = Ind_buff;
}

bool AAhatSolver::update(double * const AAhat, const int ldaah) {

    if (!is_input_ok_()) {return false;}

    // 1.0  Copy from X maxvol-rows in X_box with transpose.  |+
    for (int i = 0; i < r_; ++i) {
        cblas_dcopy(r_, &A_[Index_[i] * lda_], 1, &AAhat[i], r_);
    }

    // 2.0  Copy from X to X^T.  |+
    for (int i = 0; i < r_; ++i) {
        cblas_dcopy(n_, &A_[i], lda_, &A_buff_[i * n_], 1);
    }

    // 3.0  Find y from X_box^T * y = X^T. |+
    int err = LAPACKE_dgesv(LAPACK_ROW_MAJOR, r_, n_, AAhat, r_, I_buff_, A_buff_, n_);
    if (err) {
        cout << "AAhatSolver: Error code = " << err << endl;
        return false;
    }

    // 4.0  Copy from y to XXhat = y^T. |+
    for (int i = 0; i < r_; ++i) {
        cblas_dcopy(n_, &A_buff_[i * n_], 1, &AAhat[i], ldaah);
    }

    return true;
}

bool AAhatSolver::is_input_ok_() const {

    if ((n_ < 2) || (r_ < 1) || (lda_ < r_)) {
        cout << "Wrong input in AAhat solver!" << endl;
        return false;
    }
    return (A_ && Index_ && A_buff_ && I_buff_);
}

///////////////////////////////////////////////////////////////////////////////

SchurComplement::SchurComplement():
A_(0),
a_sub_(0),
n_(0),
r_(0),
r_sub_(0),
lda_A_(0),
lda_asub_(0),
Index_(0),
buffer_(0) {

}

SchurComplement::~SchurComplement() {

    buffer_ = 0;
    A_      = 0;
    a_sub_  = 0;
    Index_  = 0;
}

void SchurComplement::set_A(const double * const A_in) {
    A_ = A_in;
}

void SchurComplement::set_A_cols_short_size(const int r1) {
    r_ = r1;
}

void SchurComplement::set_A_cols_capacity(const int lda_A_in) {
    lda_A_ = lda_A_in;
}

void SchurComplement::set_a_sub(const double * const a_sub_in) {
    a_sub_ = a_sub_in;
}

void SchurComplement::set_a_sub_cols_short_size(const int r2) {
    r_sub_ = r2;
}

void SchurComplement::set_a_sub_cols_capacity(const int lda2_asub_in) {
    lda_asub_ = lda2_asub_in;
}

void SchurComplement::set_rows_long_size(const int n_in) {
    n_ = n_in;
}

void SchurComplement::set_Index(const int * const a_Index) {
    Index_ = a_Index;
}

void SchurComplement::set_buffer(double * const Buffer) {
    buffer_ = Buffer;
}

bool SchurComplement::const_update(double * const S) {

    if (!is_input_ok_()) {return false;}

    // 1. Prepare u[ind, :], size is r x r_sub.
    for (int i = 0; i < r_; ++i) {
        cblas_dcopy(r_sub_, &a_sub_[Index_[i] * lda_asub_], 1, &buffer_[i * r_sub_], 1);
    }

    // 2. Copy u in S.
    for (int j = 0; j < r_sub_; ++j) {
        cblas_dcopy(n_, &a_sub_[j], lda_asub_, &S[j], lda_asub_);
    }

    // 3. u - UUhat * u[ind,:]
    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, n_, r_sub_,
                r_, -1.0, A_, lda_A_, buffer_, r_sub_, 1.0, S, lda_asub_);

    return true;
}

bool SchurComplement::fast_update(double * const S) {

    a_sub_ = S;

    // 1. Prepare u[ind, :], size is r x r_sub.
    for (int i = 0; i < r_; ++i) {
        cblas_dcopy(r_sub_, &a_sub_[Index_[i] * lda_asub_], 1, &buffer_[i * r_sub_], 1);
    }

    // 3. S = u - UUhat * u[ind,:]
    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, n_, r_sub_,
                r_, -1.0, A_, lda_A_, buffer_, r_sub_, 1.0, S, lda_asub_);

    return true;
}

bool SchurComplement::is_input_ok_() const {

    if ((n_ < 2) || (r_ < 1) || (r_sub_ < 1) || (lda_A_ < r_) || (lda_asub_ < r_sub_)) {
        cout << "Wrong input in Schur compliment!" << endl;
        return false;
    }
    return (A_ && a_sub_ && Index_ && buffer_);
}

///////////////////////////////////////////////////////////////////////////////

IndexAppend::IndexAppend():
Uncopyable(),
add_Index_(0),
add_size_(0) {
}

IndexAppend::~IndexAppend() {
    add_Index_ = 0;
}

void IndexAppend::set_add_Index(const int * const add_index) {
    add_Index_ = add_index;
}

void IndexAppend::set_add_size(const int add_Size) {
    add_size_ = add_Size;
}

bool IndexAppend::update(int * const I_index, const int I_size) {

    if (!add_Index_ || !I_index) {
        cout << "IndexAppend: Error in update(...)" << endl;
        return false;
    }

    // Update indexes.
    for (int i = 0; i < add_size_; ++i) {
        I_index[I_size + i] = add_Index_[i];
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////

SubmatrixAppend::SubmatrixAppend():
a_sub_(0),
n_(0),
r_(0),
lda_(0),
r_inc_(0),
ldsub_(0) {
}

SubmatrixAppend::~SubmatrixAppend() {
    a_sub_ = 0;
}

void  set_asub_cols_capacity(const int ldsub);


void SubmatrixAppend::set_rows_long_size(const int n_in) {
    n_ = n_in;
}

void SubmatrixAppend::set_A_cols_short_size(const int r1) {
    r_ = r1;
}

void SubmatrixAppend::set_A_cols_capacty(const int lda_in) {
    lda_ = lda_in;
}

void SubmatrixAppend::set_asub(const double * const a_sub_in) {
    a_sub_ = a_sub_in;
}

void SubmatrixAppend::set_asub_cols_short_size(const int r_inc_in) {
    r_inc_ = r_inc_in;
}

void SubmatrixAppend::set_asub_cols_capacity(const int ldsub_in) {
    ldsub_ = ldsub_in;
}

bool SubmatrixAppend::update(double * const A_in) {

    if (!a_sub_ || !A_in || (lda_ < r_) || (ldsub_ < r_inc_)) {
        cout << "Error in SubmatrixAppend::update(...)" << endl;
        return false;
    }

    for (int i = 0; i < r_inc_; ++i) {
        cblas_dcopy(n_, &a_sub_[i], ldsub_, &A_in[r_ + i], lda_);
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////

ColumnUpdater::ColumnUpdater():
Uncopyable(),
schur_cmpl_(new SchurComplement),
mxvl_engine_(0),
aahat_solver_(new AAhatSolver),
appnd_asub_(new SubmatrixAppend),
AA_(0),
a_sub_(0),
n_(0),
r_(0),
lda_(0),
r_sub_(0),
ldsub_(0),
Index_(0),
buffer_(0) {

}

ColumnUpdater::~ColumnUpdater() {

    delete schur_cmpl_;
    delete aahat_solver_;
    delete appnd_asub_;

    mxvl_engine_ = 0;

    AA_     = 0;
    a_sub_  = 0;
    Index_  = 0;
    buffer_ = 0;
}

void ColumnUpdater::set_a_sub(const double * const a_Sub) {
    a_sub_ = a_Sub;
}

void ColumnUpdater::set_Index(const int * const ind_in) {
    Index_ = ind_in;
}

void ColumnUpdater::set_A_cols_short_size(const int r1) {
    r_ = r1;
}

void ColumnUpdater::set_A_cols_capacity(const int ldA_in) {
    lda_ = ldA_in;
}

void ColumnUpdater::set_a_sub_cols_short_size(const int r2) {
    r_sub_ = r2;
}

void ColumnUpdater::set_a_sub_cols_capacity(const int ld_asub_in) {
    ldsub_ = ld_asub_in;
}

void ColumnUpdater::set_rows_long_size(const int n_in) {
    n_ = n_in;
}

void ColumnUpdater::set_buffers(double * const buff_1,
                                double * const buff_2,
                                double * const buff_3) {

    if (!buff_1 || !buff_2 || !buff_3) {
        cout << "Error in ColumnUpdater: set_buffers" << endl;
    }

    buffer_  = buff_1;
    SS_buff_ = buff_2;
    buff_S_  = buff_3;
}

void ColumnUpdater::set_Ibuffer(int * const I_buffer) {
    ind_buff_ = I_buffer;
}

void ColumnUpdater::set_maxvol_solver(MaxvolEngine * const mxvlEngine) {
    mxvl_engine_ = mxvlEngine;
}

bool ColumnUpdater::update(double * const AA, int * const index_add) {

    //  S = u - np.dot(UU, u[ind,:])
    schur_cmpl_->set_A(AA);
    schur_cmpl_->set_A_cols_short_size(r_);
    schur_cmpl_->set_A_cols_capacity(lda_);
    schur_cmpl_->set_rows_long_size(n_);
    schur_cmpl_->set_a_sub(a_sub_);
    schur_cmpl_->set_a_sub_cols_short_size(r_sub_);
    schur_cmpl_->set_a_sub_cols_capacity(ldsub_);
    schur_cmpl_->set_Index(Index_);
    schur_cmpl_->set_buffer(buffer_);
    schur_cmpl_->const_update(buff_S_);

    // Remove zero lines from S.
    reduce_Schur_0_(buff_S_);

    // SShat^{-1} = QR (Qhat R)^{-1} = Q R R^{-1} Qhat^{-1} = Q Qhat^{-1}
    QR_of_Schur_complement_(n_ - r_, r_sub_, buff_S_, ldsub_);

    //  ind_add = maxvol(S)
    mxvl_engine_->set_rows_long_size(n_ - r_);
    mxvl_engine_->set_columns_short_size(r_sub_);
    mxvl_engine_->set_columns_capacity(ldsub_);
    mxvl_engine_->set_matrix_pointer(buff_S_);
    if (!mxvl_engine_->are_all_params_setup_properly()) {
        cout << "Maxvol error in Schur complement!" << endl;
        return false;
    }
    mxvl_engine_->find_maximum_volume(index_add);

    // SS = np.linalg.solve(S[ind_add, :].T, S.T).T
    aahat_solver_->set_A_pointer(buff_S_);    // S == AA
    aahat_solver_->set_rows_long_size(n_ - r_);
    aahat_solver_->set_cols_short_size(r_sub_);
    aahat_solver_->set_cols_capacity(ldsub_);
    aahat_solver_->set_maxvol_Index(index_add);
    aahat_solver_->set_matrix_buffer(buffer_);
    aahat_solver_->set_Index_buffer(ind_buff_);
    aahat_solver_->update(SS_buff_, ldsub_);

    // Inverse reduce. Inserts zero lines back.
    inverse_reduce_(SS_buff_, index_add);

    index_map_.clear();
    low_0_index_.clear();

    // U1 = UU - np.dot(SS, UU[ind_add])
    schur_cmpl_->set_rows_long_size(n_);
    schur_cmpl_->set_A(SS_buff_);
    schur_cmpl_->set_A_cols_short_size(r_sub_);
    schur_cmpl_->set_A_cols_capacity(ldsub_);
    schur_cmpl_->set_a_sub(0);
    schur_cmpl_->set_a_sub_cols_short_size(r_);
    schur_cmpl_->set_a_sub_cols_capacity(lda_);
    schur_cmpl_->set_Index(index_add);
    schur_cmpl_->set_buffer(buffer_);
    schur_cmpl_->fast_update(AA);

    // concatenate(U1, SS)
    appnd_asub_->set_rows_long_size(n_);
    appnd_asub_->set_A_cols_short_size(r_);
    appnd_asub_->set_A_cols_capacty(lda_);
    appnd_asub_->set_asub(SS_buff_);
    appnd_asub_->set_asub_cols_short_size(r_sub_);
    appnd_asub_->set_asub_cols_capacity(ldsub_);
    appnd_asub_->update(AA);

    return true;
}

void ColumnUpdater::reduce_Schur_0_(double * const S) {

    // 1. Divide maxvol index set on to parts:
    //    indexes below n - r (to be truncated)
    //    and the above (to be kept on).

    set<int> keep_ind_set;
    set<int> trnc_ind_set;
    const int razor_ind = n_ - r_;

    for (int i = 0; i < r_; ++i) {

        const int mvi = Index_[i];  // MaxVol Index
        if (razor_ind > mvi) {
            keep_ind_set.insert(mvi);
        } else {
            trnc_ind_set.insert(mvi);
        }
    }

    // 2. Copy all lines below and equal to razor_ind
    //    above the razor_ind.
    for (int i = 0; i < r_; ++i) {

        const int low_line = n_ - i - 1;
        // Try to find this index in below
        set<int>::iterator low_itr = trnc_ind_set.find(low_line);

        // line low_line is not maxvol then copy it up
        if (trnc_ind_set.end() == low_itr) {

            set<int>::iterator up_itr = keep_ind_set.begin();
            if (up_itr != keep_ind_set.end()) {

                // copy line from below to the top.
                const int up_line = *up_itr;
                cblas_dcopy(r_sub_, &S[low_line * ldsub_], 1, &S[up_line * ldsub_], 1);

                // save the couple of indexes (to unroll in future).
                index_map_.insert(std::pair<int, int >(up_line, low_line));

                keep_ind_set.erase(up_itr);
            } else {
                cout << "Up index set Error 23" << endl;
            }
        } else {

            trnc_ind_set.erase(low_itr);
            low_0_index_.insert(low_line);
        }
    }
}

bool ColumnUpdater::QR_of_Schur_complement_(const int n, const int r,
                                            double * const S, const int lds) {
    const int ldu = lds;
    double * const pU  = S;
    double * const tau = buffer_;

    // 1. QR via Hausholder. Especial storage format.
    int ok = LAPACKE_dgeqrf(LAPACK_ROW_MAJOR, n, r, pU, ldu, tau);
    if (ok) {
        cout << "ColumnUpdater: QR1 error! code = " << ok << endl;
        return false;
    }

    // 2. Restore Q from lapack format.
    ok = LAPACKE_dorgqr(LAPACK_ROW_MAJOR, n, r, r, pU, ldu, tau);
    if (ok) {
        cout << "ColumnUpdater: QR2 error! code = " << ok << endl;
        return false;
    }

    return false;
}

void ColumnUpdater::inverse_reduce_(double * const SShat, int * const index_add) {

    // 1.  Maxvol replace;
    std::map<int, int >::iterator itrf;
    for (int i = 0; i < r_sub_; ++i) {

        itrf = index_map_.find(index_add[i]);
        if (itrf != index_map_.end()) {
            index_add[i] = itrf->second;
        }
    }

    // 2.  Copy
    std::map<int, int >::iterator  mitr;
    for (mitr = index_map_.begin(); mitr != index_map_.end(); ++mitr) {

        const int up_line  = mitr->first;
        const int low_line = mitr->second;

        // 1.1  Copy up-line to the low extended line
        cblas_dcopy(r_sub_, &SShat[up_line * ldsub_], 1, &SShat[low_line * ldsub_], 1);

        // 1.2  Nullify up-line in the matrix.
        cblas_dscal(r_sub_, 0.0, &SShat[up_line * ldsub_], 1);
    }

    // 3. Nullify low lines in the extended matrix.
    std::set<int>::iterator itrs;
    for (itrs = low_0_index_.begin(); low_0_index_.end() != itrs; ++itrs) {

        const int l_ind = (*itrs);
        cblas_dscal(r_sub_, 0.0, &SShat[l_ind * ldsub_], 1);
    }
}

///////////////////////////////////////////////////////////////////////////////

ProximityEstimator::ProximityEstimator():
rank_(0),
rank_inc_(0),
UUhat_(0),
VVhat_(0),
u_sub_(0),
v_sub_(0),
Abox_(0),
buff_1_(0),
buff_2_(0),
buff_3_(0),
row_index_(0),
col_index_(0),
row_inc_index_(0),
col_inc_index_(0) {

}

ProximityEstimator::~ProximityEstimator() {

    UUhat_  = 0;
    VVhat_  = 0;
    u_sub_  = 0;
    v_sub_  = 0;
    Abox_   = 0;
    buff_1_ = 0;
    buff_2_ = 0;
    buff_3_ = 0;
    row_index_     = 0;
    col_index_     = 0;
    row_inc_index_ = 0;
    col_inc_index_ = 0;
}

bool ProximityEstimator::is_least_rank_detection(const double Eps) {

    // 1.   SVD decomposition; buff1, buff2, buff3 are free here for use.

    // 1.1  Input data.
    const int r = rank_;
    double * const A = buff_1_->adress();  // Matrix A for SVD.
    double * const S = buff_2_->adress();
    double * const buffer = buff_3_->adress();

    // 1.2  Copy Abox.
    double * const pAb = Abox_->adress();
    const int cap = Abox_->capacity();
    for (int i = 0; i < r; ++i) {
        cblas_dcopy(r, &pAb[i * cap], 1, &A[i * r] ,1);
    }

    // 1.3  SVD.
    const bool ok = LAPACKE_dgesvd(LAPACK_ROW_MAJOR, 'N', 'N', r, r, A, r,
                                                   S, 0, r, 0, r, buffer);
    if (ok) {
        cout << "SchurCross2D: SVD rounding error! code = " << ok << endl;
        return false;
    }

    // 2. Rounding.
    int rounded_rank = 1;
    const double s0 = S[0];
    for (int i = 1; i < r; ++i) {

        if (S[i] > (Eps * s0)) {
            rounded_rank += 1;
        } else {
            break;
        }
    }

    // 3. Test the exit condition.
    if (rounded_rank < rank_) {
        return true;
    }
    return false;
}

bool ProximityEstimator::is_approximation_quite_good(const double Eps_stop) {

    if (!is_input_ok_()) return false;

    // Check u_sub approximation.
    // 1.1. Fill up Abox matrix which is a cross of U and V = f(ind_1(i), ind_2(j)).
    double * const Abox_u = buff_1_->adress();
    if (!fill_up_Abox_(Abox_u)) {return false;}

    // 1.2. UUhat x Abox.
    double * const UUhat_Abox = buff_2_->adress();
    matrix_x_Abox_(UUhat_, Abox_u, rank_, CblasNoTrans, UUhat_Abox);

    // 1.3. Produce rows-selected VVhat^T
    double * const rsVVhatT = buff_3_->adress();
    make_rows_selected_AAhatT_(VVhat_, col_inc_index_, rsVVhatT);

    // 1.4. Multiply UUhat_Abox and rsVVhatT
    double * const u_approx = buff_1_->adress();  // Abox is not needed for now.
    const int n = UUhat_->num_rows();
    mult_AAhat_Abox_x_AAadd_(UUhat_Abox, n, rsVVhatT, u_approx);

    // 1.5. Calculate relative error for u_approx.
    const double eps_u = compare_norm2_(u_sub_, u_approx, n, buff_2_->adress(), buff_3_->adress());


    // Check v_sub approximation.
    // 2.1 Fill up Abox matrix again, see 1.1. No transpose here.
    double * const Abox_v = buff_1_->adress();
    if (!fill_up_Abox_(Abox_v)) {return false;}

    // 2.2. VVhat x Abox^T.
    double * const VVhat_AboxT = buff_2_->adress();
    matrix_x_Abox_(VVhat_, Abox_v, rank_, CblasTrans, VVhat_AboxT);

    // 2.3. Produce rows-selected UUhat^T
    double * const rsUUhatT = buff_3_->adress();
    make_rows_selected_AAhatT_(UUhat_, row_inc_index_, rsVVhatT);

    // 2.4. Multiply VVhat_Abox and rsUUhatT
    double * const v_approx = buff_1_->adress();  // Abox is not needed at this moment.
    const int m = VVhat_->num_rows();
    mult_AAhat_Abox_x_AAadd_(VVhat_AboxT, m, rsUUhatT, v_approx);

    // 2.5. Calculate relative error for v_approx.
    const double eps_v = compare_norm2_(v_sub_, v_approx, m, buff_2_->adress(), buff_3_->adress());


    // 3.0. Main comparision.
//    const double uv_eps = std::max<double>(eps_u, eps_v);
    const double uv_eps = 0.5 * (eps_u + eps_v);

    // *** *** *** Approximation is quite good *** *** ***
    if (uv_eps < Eps_stop) {return true;}
    // *** *** *** *** *** *** *** *** *** *** *** *** ***

    return false;
}

bool ProximityEstimator::fill_up_Abox_(double * const Abox_io) {

    double * const pAb = Abox_->adress();
    const int lda = Abox_->capacity();

    for (int i = 0; i < rank_; ++i) {
        cblas_dcopy(rank_, &pAb[i], lda, &Abox_io[i], rank_);
    }

    return true;
}

void ProximityEstimator::update_rank(const int Rank_in) {
    rank_ = Rank_in;
}

void ProximityEstimator::set_inc_rank(const int incRank) {
    rank_inc_ = incRank;
}

void ProximityEstimator::set_UUhat(BaseArrayHolder<double> * const UUhat_in) {
    UUhat_ = UUhat_in;
}

void ProximityEstimator::set_VVhat(BaseArrayHolder<double> * const VVhat_in) {
    VVhat_ = VVhat_in;
}

void ProximityEstimator::set_u_sub(BaseArrayHolder<double> * const u_sub_in) {
    u_sub_ = u_sub_in;
}

void ProximityEstimator::set_v_sub(BaseArrayHolder<double> * const v_sub_in) {
    v_sub_ = v_sub_in;
}

void ProximityEstimator::set_Abox(BaseArrayHolder<double> * const Abox_in) {
    Abox_ = Abox_in;
}

void ProximityEstimator::set_buffers(BaseArrayHolder<double> * const buff_1_in,
                                     BaseArrayHolder<double> * const buff_2_in,
                                     BaseArrayHolder<double> * const buff_3_in) {
    buff_1_ = buff_1_in;
    buff_2_ = buff_2_in;
    buff_3_ = buff_3_in;
}

void ProximityEstimator::set_row_index(BaseArrayHolder<int> * const row_index_in) {
    row_index_ = row_index_in;
}

void ProximityEstimator::set_col_index(BaseArrayHolder<int> * const col_index_in) {
    col_index_ = col_index_in;
}

void ProximityEstimator::set_row_inc_index(BaseArrayHolder<int> * const row_inc_ind_in) {
    row_inc_index_ = row_inc_ind_in;
}

void ProximityEstimator::set_col_inc_index(BaseArrayHolder<int> * const col_inc_ind_in) {
    col_inc_index_ = col_inc_ind_in;
}

bool ProximityEstimator::is_input_ok_() {

    const bool ok_d1 = //static_cast<bool>(U_) && static_cast<bool>(V_) &&
                       static_cast<bool>(UUhat_) && static_cast<bool>(VVhat_) &&
                       static_cast<bool>(u_sub_) && static_cast<bool>(v_sub_);

    const bool ok_d2 = static_cast<bool>(buff_1_) && static_cast<bool>(buff_2_) &&
                       static_cast<bool>(buff_3_);

    const bool ok_i1 = static_cast<bool>(row_index_) && static_cast<bool>(col_index_) &&
                       static_cast<bool>(row_inc_index_) && static_cast<bool>(col_inc_index_);

    if (ok_d1 && ok_d2 && ok_i1) {
        return true;
    }

    cout << "ProximityEstimator Error!";
    cout << "There is at least one zero pointer." << endl;

    return false;
}

void ProximityEstimator::matrix_x_Abox_(BaseArrayHolder<double> * const AAhat,
                                        const double * const Abox,
                                        const int sizeA,
                                        const enum CBLAS_TRANSPOSE TransAbox,
                                        double * const buffer_rezlt) {

    cblas_dgemm(CblasRowMajor, CblasNoTrans, TransAbox, AAhat->num_rows(),
                sizeA, sizeA, 1.0, AAhat->adress(), AAhat->capacity(),
                Abox, sizeA, 0.0, buffer_rezlt, sizeA);
}

void ProximityEstimator::make_rows_selected_AAhatT_(BaseArrayHolder<double> * const AAhat,
                                                    BaseArrayHolder<int> * const inc_index,
                                                    double * const VV_add) {
    double * const pV = AAhat->adress();
    const int ldv = AAhat->capacity();
    const int inc_sz = inc_index->col_size();
    const int rnk_sz = AAhat->col_size();

    for (int i = 0; i < inc_sz; ++i) {
        const int indv_i = inc_index->adress()[i];
        cblas_dcopy(rnk_sz, &pV[indv_i * ldv], 1, &VV_add[i], inc_sz);
    }
}

void ProximityEstimator::mult_AAhat_Abox_x_AAadd_(double * const UUhat_Abox,
                                                  const int n,
                                                  double * const VV_add,
                                                  double * const a_approx) {

    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,
                n, rank_inc_, rank_, 1.0, UUhat_Abox, rank_,
                VV_add, rank_inc_, 0.0, a_approx, rank_inc_);
}

double ProximityEstimator::compare_norm2_(BaseArrayHolder<double> * const a_sub,
                                          double * const a_approx, const int rows_size_n,
                                          double * const nrm_vec, double * const nrm_dif) {

    // (u_approx = u_approx - u) for every column.
    const int n = rows_size_n;
    double * pa_sub = a_sub->adress();
    const int ldsub = a_sub->capacity();

    for (int k = 0; k < rank_inc_; ++k) {
        cblas_daxpy(n, -1.0, &pa_sub[k], ldsub, &a_approx[k], rank_inc_);
        nrm_vec[k] = cblas_dnrm2(n, &pa_sub[k], ldsub);
        nrm_dif[k] = cblas_dnrm2(n, &a_approx[k], rank_inc_);
//        cout << "nrm_vec = " << nrm_vec[k] << " nrm_dif = " << nrm_dif[k] << endl;
    }

    const double tot_nrm_vec = cblas_dnrm2(rank_inc_, nrm_vec, 1);
    const double tot_nrm_dif = cblas_dnrm2(rank_inc_, nrm_dif, 1);

//    cout << "dif = "<< tot_nrm_dif << endl;
//    cout << "tot = "<< tot_nrm_vec << endl;

    if (tot_nrm_vec < 1e-300) {
        cout << "ProximityEstimator warning! vector norm is less then 1e-300" << endl;
        return 0.0;
    }
    return tot_nrm_dif / tot_nrm_vec;
}

///////////////////////////////////////////////////////////////////////////////

// End of the file

