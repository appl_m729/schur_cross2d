#ifndef _SCHUR_CROSS_2D_H_
#define _SCHUR_CROSS_2D_H_

#include "uncopyable.h"
#include "array_holder.h"

class TargetFunction;
class MaxvolEngine;
class AAhatSolver;
class ColumnUpdater;
class SubmatrixAppend;
class IndexAppend;
class ProximityEstimator;

///////////////////////////////////////////////////////////////////////////////

/*!  \class SchurCross2D decompose a given matrix A
 *   into the cross approximation A = U * S * V^T
 *   using fast Schur complementation update.
 *
 *   It uses TargetFunction class and maxvol subroutine.
 *
 *   Version      sn-1.0.1, 14-11-2014
 *                sn-1.2.0, 21-11-2014
 *                sn-1.3.0, 04-12-2014   (row rank = col rank)
 *                sn-1.4.0, 26-12-2014   (Approximation does work)
 *                sn-1.4.1. 04-02-2014   (Least rank detection is fixed)
 */

class SchurCross2D : public Uncopyable {
public:

    SchurCross2D();
    ~SchurCross2D();

    // Target function to be approximated.
    // Must be called.
    bool   set_target_function(TargetFunction * const targetFunc);

    // One-step incrementation of rank for rows or columns
    // (matrices V^T and U grow up correspondingly).
    // Must be called.
    bool   set_rank_increment(const int rank_inc);

    // Singular values less then epsilon are thrown out.
    // Must be called.
    bool   set_rank_truncation(const double epsilon);

    // Main approximation loop. Controls divergence behaviour.
    // Default value is ok.
    bool   set_iter_limit(const int max_iter = 2014);

    // Sets up the tolerance of the maxvol subroutine.
    // Default value is ok.
    bool   set_maxvol_tolerance(const double tolerance);
    bool   set_maxvol_iter_limit(const int max_iter);

    // Call this routine before calling find_approximate_decomposition()
    bool   are_all_params_setup_properly() const;

    // Schur 2d cross expansion (O(n * r^2) complexity).
    bool   find_approximate_decomposition();

    // Calculates approximated (i,j) - value of
    // the target function by the cross decomposition.
    // It is product of the i-th line and j-th coulumn of
    // approximant matrices. O(r) complexity.
    double approx_value(const int i, const int j);

    // 2D-cross decomposition  A = U * Sigma * Vt
    // U is n x r, Vt is r x m, Sigma is diagonal of length r.
    // U * sqrt(Sigma)
    const double * U_adress() const;
    int ld_U() const;

    // V * sqrt(Sigma), not transposed!
    const double * V_adress() const;
    int ld_V() const;

    // The cross rank.
    int rank() const {return rank_;}

    // Relative error.
    double accuracy() const {return eps_rank_;}

private:

    // Measure the whole time of decomposition.
    const clock_t t_start_;

    // Target function to be approximated. Includes number of
    // matrix rows and columns, mesh bounds and mesh data.
    TargetFunction * tfunc_;

    // Maxvol pointer
    MaxvolEngine * const mxvl_engine_;

    // Finds product A * A_hat^(-1)
    AAhatSolver * const aahatSlvr_;

    // Updates columns with Schur compliment.
    ColumnUpdater * const clmnUpd_;

    // Adds columns to a given matrix.
    SubmatrixAppend * const submApnd_;

    // Adds set of incremented index from maxvol to Index set.
    IndexAppend * const indApnd_;

    // Estimates quality of approximation for u and v
    // and stops main loop if converged.
    ProximityEstimator * const uv_approx_;

    // Controlls proper input.
    bool rank_increment_flag_;
    bool rank_truncation_flag_;

    // Input data.
    int    iter_limit_;  // Maximum iterations limit.
    int    rank_inc_;    // Rank increment for rows and cols.
    double eps_rank_;    // Rank truncation. Relative accuracy.

    // Dynamically increases while the loop goes
    int    rank_;        // Actual rank (rows and cols size of approximant)

    // Data arrays
    BaseArrayHolder<double> * const UUhat_;    // U * U_hat.
    BaseArrayHolder<double> * const VVhat_;    // V * V_hat.
    BaseArrayHolder<double> * const u_sub_;    // Schur submatrix for maxvol search. All rows x rank cols.
    BaseArrayHolder<double> * const v_sub_;    // Schur submatrix for maxvol search. All cols, rank rows.
    BaseArrayHolder<double> * const Abox_;     // An intersection of "good" columns and rows.
    BaseArrayHolder<double> * const buff_1_;   // double buffers for U and V
    BaseArrayHolder<double> * const buff_2_;
    BaseArrayHolder<double> * const buff_3_;

    // Just pointers. They are initialized after all calculations are done
    // that is the approximation is converged.
    // The final truncated U is in buff_1_ and
    // the final truncated V is in UUhat_.
    // These pointers point out to these buffers.
    BaseArrayHolder<double> * Ur_;  // Columns approximant points out to buff_1_
    BaseArrayHolder<double> * Vr_;  // Rows approximant points out to UUhat_

    // Maxvol indexes.
    BaseArrayHolder<int> * const row_index_;
    BaseArrayHolder<int> * const col_index_;
    BaseArrayHolder<int> * const row_inc_index_;
    BaseArrayHolder<int> * const col_inc_index_;
    BaseArrayHolder<int> * const I_buff_;

    // If num rows not eq. to num cols, possible rank
    // can exceed the minimal of these values.
    bool  check_size_rank_condition_();

    // Reallocates memory for all array-holders.
    bool  update_memory_(const int newRank);

    // Fill the input matrix by random numbers from [-1, 1].
    bool   random_init_(BaseArrayHolder<double> * const U);

    // Orthogonalize vectors in A by QR. A <-- Q. tau is just a temporary bufer.
    bool   QR_of_A_(BaseArrayHolder<double> * const U);

    // Maxvol for submatrix A.
    bool   find_maxvol_indexes_(BaseArrayHolder<double> * const U,
                                BaseArrayHolder<int> * const Index);

    // Calculate n * r1 values in vertical maxvol submatrix u.
    // Write A(i, col_Index_[j]) in u(i,j); 0 <= i < n, 0 < j < r1.
    void   calc_cols_submatrix_(const int * const col_Index);

    // Calculate m * r2 values in horizontal maxvol submatrix.
    // Write A(row_Index_[i], j) in v(i,j);  0 <= i < r2, 0 < j < m.
    void   calc_rows_submatrix_(const int * const row_Index);

    // Wrapper for UUhat and VVhat solvers.
    bool   solve_AAhat_(BaseArrayHolder<double> * const U,
                        BaseArrayHolder<double> * const UUhat,
                        BaseArrayHolder<int> * const Index);

    // Add columns via Schur compliment.
    bool   column_update_(BaseArrayHolder<double> * const UUhat,
                          BaseArrayHolder<double> * const u_sub,
                          BaseArrayHolder<int> * const Index,
                          BaseArrayHolder<int> * const incInd);

    // Appends submatrix (columns) to AAhat (to its right-hand-side).
    bool   append_asub_to_A_(BaseArrayHolder<double> * const AAhat,
                             BaseArrayHolder<double> * const a_sub);

    // Appends new (incremented) maxvol indeces (to its right-hand-side).
    bool   append_inc_Index_(BaseArrayHolder<int> * const Index,
                             BaseArrayHolder<int> * const incInd);

    // Initial setup of Abox. Its size is rank_inc_ x rank_inc_.
    void   init_Abox_();

    // Add new rows and cols to the core Abox.
    bool   update_Abox_();

    // Check if rank of the matrix < rank_increment.
    bool   forward_least_rank_detection_();

    // Compare approximation accuracy.
    bool   is_approximation_quite_good_(const int k_iter);

    // Final rounding of good "vectors".
    void   QR_orthogonalization_with_SVD_rounding_();

    // QR decomposition of AAhat with R returned.
    // Attention: buff_3_ is used here!
    bool   final_QR_of_AAhat_(BaseArrayHolder<double> * const AAhat,
                              BaseArrayHolder<double> * const its_R);

    // Calc matrix for final SVD rounding.
    bool   calc_Ru_Abox_RvT_product_(BaseArrayHolder<double> * const Ru,
                                     BaseArrayHolder<double> * const Rv,
                                     double * const G_prod);
    // SVD of product Ru * Abox * T(Rv)
    int    SVD_round_of_(double * const G, const int r,
                         double * const U, double * const S,
                         double * const V, double * const buffer);

    // Square of the singular-values-tail over sq. of s.-v.-head
    int    rounded_rank_(const int full_rank,  double * S,
                         double * const buff);

    // Final multiplication of Qu * Ru after QR and SVD.
    bool   mult_Qa_Ra_(BaseArrayHolder<double> * const Ur,
                       BaseArrayHolder<double> * const UUhat,
                       double * const Usvd,
                       const enum CBLAS_TRANSPOSE TransA);

    void   push_sqrt_sigma_in_(BaseArrayHolder<double> * const Ain,
                                               double  * const Sigma);

    bool   check_update_error_(const int err, const int k_iter);
};

///////////////////////////////////////////////////////////////////////////////

#endif   // _SCHUR_CROSS_2D_H_

// End of the file

