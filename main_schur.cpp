#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include "schur_cross_2d.h"
#include "target_function.h"
#include "matrix_tools.h"

using std::cout;
using std::endl;
using std::setw;
using std::setprecision;

///////////////////////////////////////////////////////////////////////////////

int main(int argc, char * argv[]) {

    TargetFunction gfun;
    TargetFunction * tFunc = &gfun;

    const int n = 1000;
    const int m = 1000;
    const double eps = 1e-9;

    gfun.set_n_rows(n);
    gfun.set_m_cols(m);
    gfun.set_ax_left_bound(-10.0);
    gfun.set_bx_right_bound(20.0);
    gfun.set_ay_left_bound(-20.0);
    gfun.set_by_right_bound(10.0);

    SchurCross2D cross2d;
    SchurCross2D * schr_cross_2d = &cross2d;

    schr_cross_2d->set_maxvol_tolerance(1e-2);
    schr_cross_2d->set_target_function(tFunc);
    schr_cross_2d->set_rank_increment(4);
    schr_cross_2d->set_rank_truncation(eps);
    schr_cross_2d->set_iter_limit(2014);

    if (schr_cross_2d->are_all_params_setup_properly()) {
        const bool ok_cross = schr_cross_2d->find_approximate_decomposition();
        if (!ok_cross) {
            cout << "SchurCross2D error! Approximation failed." << endl;
            return 0;
        }
    } else {
        cout << "SchurCross2D: Error in input params." << endl;
        return 0;
    }

    int num_min_err = 0;
    int num_max_err = 0;
    int i_err_min = 0, j_err_min = 0;
    int i_err_max = 0, j_err_max = 0;
    int i_val_min = 0, j_val_min = 0;
    int i_val_max = 0, j_val_max = 0;
    double f_val_min = fabs(gfun.value(i_val_min, j_val_min));
    double f_val_max = fabs(gfun.value(i_val_max, j_val_max));
    double err_max = 0;
    double err_min = 1000000;

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {

            double val_ij = cross2d.approx_value(i,j);
            const double eta = fabs((gfun.value(i,j) - val_ij) / gfun.value(i,j));
            if (eta > eps && fabs(gfun.value(i,j)) > 1e-100) {
                ++num_max_err;
            }
            if (err_max < eta) {
                err_max = eta;
                i_err_max = i;
                j_err_max = j;
            }
            if (eta < eps && fabs(gfun.value(i,j)) > 1e-100) {
                ++num_min_err;
            }
            if (err_min > eta) {
                err_min = eta;
                i_err_min = i;
                j_err_min = j;
            }

            const double fv_ij = gfun.value(i,j);
            if (f_val_max < fv_ij) {
                f_val_max = fv_ij;
                i_val_max = i;
                j_val_max = j;
            }
            if (f_val_min > fv_ij) {
                f_val_min = fv_ij;
                i_val_min = i;
                j_val_min = j;
            }
        }
    }

    cout << "[" << gfun.ax_left_bound() << ",";
    cout << gfun.bx_right_bound() << "] x [";
    cout << gfun.ay_left_bound() << "," << gfun.by_right_bound();
    cout << "]"<< endl;
    cout << "n = " << gfun.n_rows();
    cout << "  m = " << gfun.m_cols() << endl;
    cout << "eps_cross = " << cross2d.accuracy() << endl;

    cout << "Test passed with " << setw(11) << num_max_err << " K_max errors" << endl;
    cout << "                 " << setw(11) << num_min_err << " K_min errors" << endl;

    cout << "Max relative error = "<< setprecision(17) << setw(24) << err_max;
    cout << " against " << eps << endl;
    cout << "cross value in max err = ";
    cout << std::setprecision(17) << cross2d.approx_value(i_err_max, j_err_max) << endl;
    cout << "    f value in max err = ";
    cout << std::setprecision(17) << gfun.value(i_err_max, j_err_max) << endl;
    cout << std::setprecision(17) << "f maximum = " << f_val_max << endl;
    cout << "cross max = " << cross2d.approx_value(i_val_max, j_val_max) << endl;

    cout << "Min relative error = "<< setprecision(17) << setw(24) << err_min;
    cout << " against " << eps << endl;
    cout << "cross value in min err = ";
    cout << std::setprecision(17) << cross2d.approx_value(i_err_min, j_err_min) << endl;
    cout << "    f value in min err = ";
    cout << std::setprecision(17) << gfun.value(i_err_min, j_err_min) << endl;
    cout << std::setprecision(17) << "f minimum = " << f_val_min << endl;
    cout << "cross min = " << cross2d.approx_value(i_val_min, j_val_min) << endl;



//    const int n = tFunc->n_rows();
//    const int m = tFunc->m_cols();
//    int num_err = 0;
//    double err_max = 0.0;
//    int i0 = 0, j0 = 0;
//    double val_max = fabs(tFunc->value(0,0));
//    double val_min = fabs(tFunc->value(0,0));

//    for (int i = 0; i < n; ++i) {
//        for (int j = 0; j < m; ++j) {

//            double val_ij = schr_cross_2d->approx_value(i,j);
//            const double eta = fabs( (tFunc->value(i,j) - val_ij) / tFunc->value(i,j) );
//            if (eta > schr_cross_2d->accuracy() &&
//                fabs(tFunc->value(i,j)) > 1e-100) {
//                ++num_err;
//                if (err_max < eta) {
//                    err_max = eta;
//                    i0 = i;
//                    j0 = j;
//                }
//            }

//            if (val_max < val_ij) {
//                val_max = val_ij;
//            }
//            if (val_min > val_ij) {
//                val_min = val_ij;
//            }
//        }
//    }

//    cout << "Test passed with " << num_err << " errors" << endl;
//    cout << "Max relative error = "<< err_max;
//    cout << " against " << schr_cross_2d->accuracy() << endl;
//    cout << std::setprecision(17) << schr_cross_2d->approx_value(i0,j0) << endl;
//    cout << std::setprecision(17) << tFunc->value(i0, j0) << endl;
//    cout << "maximum = " << val_max << endl;
//    cout << "minimum = " << val_min << endl;
//    cout << "[" << tFunc->ax_left_bound() << ",";
//    cout << tFunc->bx_right_bound() << "] x [";
//    cout << tFunc->ay_left_bound() << "," << tFunc->by_right_bound();
//    cout << "]"<< endl;
//    cout << "n = " << tFunc->n_rows();
//    cout << "  m = " << tFunc->m_cols() << endl;
//    cout << "eps_cross = " << schr_cross_2d->accuracy() << endl;



//    cout << "Value:" << endl;
//    for (int i = 0; i < n; ++i) {
//        for (int j = 0; j < m; ++j) {
//            double sum = schr_cross_2d->approx_value(i,j);
//            if (sum < 1e-30) sum = 0;
////            double sum = tFunc->value(i,j);
//            cout << setw(24) << sum;
//        }
//        cout << endl;
//    }

    return 0;
}

//    const double * const pUr = schr_cross_2d->U_adress();
//    const int ldur = schr_cross_2d->ld_U();

//    const double * const pVr = schr_cross_2d->V_adress();
//    const int ldvr = schr_cross_2d->ld_V();

//        for (int i = 0; i < m; ++i) {
//            for (int j = 0; j < 1; ++j) {

//                cout << std::setw(14) << pVr[i * ldur + j];

    //            double sum_ij = 0;
    //            for (int k = 0; k < r_svd; ++k) {
    //                sum_ij += pUr[i * ldur + k] * Ssvd[k] * pVr[j * ldvr + k];
    //            }
    //            cout << setw(12) << sum;
    //            double val_ij = tfunc_->value(i, j);
    //            if (fabs(sum_ij - val_ij) > 2.0 * eps_rank_) {
    //                cout << "error at ij = " << i << " " << j << endl;
    //                cout << "sum_ij = " << std::setprecision(18) << sum_ij << endl;
    //                cout << "val_ij = " << std::setprecision(18) << val_ij << endl;
    //                return;
//    //            }
//            }
////            cout << endl;
//        }

/////////////////////////////////////////////////////////////////////////////////

// End of the file

