obj = o
CompCXX = g++-4.9 -O3 
CBLAS_PATH = ../itt++/cblas-3.5.0
LAPACKE_PATH = ../itt++/lapacke-3.5.0
INCL_PATH  = -I$(CBLAS_PATH)/include -I$(LAPACKE_PATH)/include 
LIB_PATH   = -L$(CBLAS_PATH)/lib -L$(LAPACKE_PATH)/lib
LIB_PATH  += -L/usr/local/Cellar/gcc/4.9.1/lib/gcc/x86_64-apple-darwin14.0.0/4.9.1
CompFF     = gfortran -O3 -std=f2003
LVER = _mac


code = main_schur.$(obj) maxvol_engine.$(obj) schur_cross_2d.$(obj) \
       matrix_tools.$(obj) target_function.$(obj) fmaxvol.$(obj)

main_schur$(x): $(code)
	$(CompCXX) $(code) $(LIB_PATH) -lblas -lcblas$(LVER) -llapack -llapacke$(LVER) -lgfortran

main_schur.$(obj): main_schur.cpp
	$(CompCXX) -c $(INCL_PATH) main_schur.cpp

maxvol_engine.$(obj): maxvol_engine.cpp
	$(CompCXX) -c $(INCL_PATH) maxvol_engine.cpp

schur_cross_2d.$(obj): schur_cross_2d.cpp
	$(CompCXX) -c $(INCL_PATH) schur_cross_2d.cpp

target_function.$(obj): target_function.cpp
	$(CompCXX) -c $(INCL_PATH) target_function.cpp

matrix_tools.$(obj): matrix_tools.cpp
	$(CompCXX) -c $(INCL_PATH) matrix_tools.cpp

fmaxvol.$(obj): fmaxvol.f90
	$(CompFF) -c $(INCL_PATH) fmaxvol.f90

clean:
	rm *.$(obj) a.out


# End of file


