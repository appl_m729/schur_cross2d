#ifndef _MATRIX_TOOLS_H_
#define _MATRIX_TOOLS_H_

#include <set>
#include <map>
#include "uncopyable.h"
#include "array_holder.h"
#include "cblas.h"

using std::map;
using std::set;

class MaxvolEngine;

///////////////////////////////////////////////////////////////////////////////

/*!  \class Solves A * A_hat^(-1) product using dgesv LAPACK routine.
 *   The class needs one double buffer of size n x r, and
 *   one int buffer of size n.
 *
 *   *** This module is successfully applicable for the
 *   row-like array format with a given capacity.
 *
 *   Input is a given matrix A and maxvol indexes.
 *   Output is a product of A * A_hat^(-1).
 *
 *   Version - 1.0.3, 09-12-2014.
 */

class AAhatSolver : public Uncopyable {
public:

    AAhatSolver();
    ~AAhatSolver();

    // Pointer to the given matrix. The matrix is not changed.
    void set_A_pointer(const double * const A);
    void set_rows_long_size(const int n);
    void set_cols_short_size(const int r);
    // Capacity is a leading dimension of matrix A and AAhat.
    void set_cols_capacity(const int capacity);
    void set_maxvol_Index(const int * const Index);

    // Buffer arrays for LAPACK routines.
    void set_matrix_buffer(double * const A_buff);  // size n * r
    void set_Index_buffer(int * const Ind_buff);    // size n

    // Main solver.
    bool update(double * const AAhat, const int ldaah);

private:

    const double * A_;
    int n_;
    int r_;
    int lda_;        // Capacity == leading dimension of A.
    const int * Index_;

    double * A_buff_;
    int    * I_buff_;

    bool is_input_ok_() const;
};

///////////////////////////////////////////////////////////////////////////////

/*! \class SchurCompliment finds the matrix S, where
 *  S = a_sub - dot(AA, a_sub[ind,:]).
 *  Input is A, A_sub, (maxvol of A_sub) Index.
 *  double Buffer is needed as well.
 *
 *  *** This module is successfully applicable for the
 *  row-like array format with a given capacity.
 *
 *  Version - 1.0.3, 09-12-2014.
 */

class SchurComplement : public Uncopyable {
public:

    SchurComplement();
    ~SchurComplement();

    // A is not changing.
    void  set_A(const double * const A);
    void  set_A_cols_short_size(const int r1);
    void  set_A_cols_capacity(const int lda1);

    // a_sub is not changing.
    void  set_a_sub(const double * const a_sub);
    void  set_a_sub_cols_short_size(const int r2);
    void  set_a_sub_cols_capacity(const int lda2);

    void  set_rows_long_size(const int n);

    void  set_Index(const int * const a_Index);
    void  set_buffer(double * const Buffer);

    bool  const_update(double * const S);
    bool  fast_update(double * const S);

private:

    const double * A_;
    const double * a_sub_;

    int n_;
    int r_;
    int r_sub_;
    int lda_A_;
    int lda_asub_;

    const int * Index_;
    double * buffer_;

    bool is_input_ok_() const;
};

///////////////////////////////////////////////////////////////////////////////

/*! Append indexes from new maxvol to the full "good" indexes array.
 *  The capacity of the array is supposed to be enough for new indexes.
 *
 *  Version - 1.0.3, 09-12-2014.
 */

class IndexAppend : public Uncopyable {
public:

    IndexAppend();
    ~IndexAppend();

    void set_add_Index(const int * const add_index);
    void set_add_size(const int add_size);

    bool update(int * const I_index, const int I_size);

private:

    const int * add_Index_;
    int add_size_;
};

///////////////////////////////////////////////////////////////////////////////

/*! Append new maxvol columns (rows) to the full "good" matrix (U or V).
 *  The capacity of the array is supposed to be enough for new elements.
 *  A is before-add-matrix of size n rows x r columns.
 *  r_inc - the number of added columns (rows) of length n.
 *
 *  *** This module is successfully applicable for the
 *  row-like array format with a given capacity.
 *
 *   Version - 1.0.3, 09-12-2014.
 */

class SubmatrixAppend {
public:

    SubmatrixAppend();
    ~SubmatrixAppend();

    void  set_rows_long_size(const int n);
    void  set_A_cols_short_size(const int r1);
    void  set_A_cols_capacty(const int lda);

    void  set_asub(const double * const a_sub);
    void  set_asub_cols_short_size(const int r_inc);
    void  set_asub_cols_capacity(const int ldsub);

    bool  update(double * const A);

private:

    const double * a_sub_;

    int n_;
    int r_;
    int lda_;

    int r_inc_;
    int ldsub_;
};

///////////////////////////////////////////////////////////////////////////////

/*! ColumnUpdater updates added columns of matrix UUhat^(-1)
 *  in accordance to fast Schur complement.
 *
 *  *** This module is successfully applicable for the
 *  row-like array format with a given capacity.
 *
 *  Version - 1.0.3, 09-12-2014.
 *
 */

class ColumnUpdater : public Uncopyable {
public:

    ColumnUpdater();
    ~ColumnUpdater();

    // a_sub is not changed
    void  set_a_sub(const double * const a_sub);
    // Index is not changed.
    void  set_Index(const int * const Index);

    void  set_A_cols_short_size(const int r1);
    void  set_A_cols_capacity(const int lda);
    void  set_a_sub_cols_short_size(const int r2);
    void  set_a_sub_cols_capacity(const int ldsub);
    void  set_rows_long_size(const int n);

    void  set_buffers(double * const buff_1, double * const buff_2,
                      double * const buff_3);
    void  set_Ibuffer(int * const I_buffer);

    void  set_maxvol_solver(MaxvolEngine * const mxvl_engine);

    // calculates updated and increased AA and new index_add.
    bool update(double * const AA, int * const index_add);

private:

    SchurComplement * const schur_cmpl_;
    MaxvolEngine * mxvl_engine_;
    AAhatSolver * const aahat_solver_;
    SubmatrixAppend * const appnd_asub_;

    double * AA_;
    const double * a_sub_;

    int n_;
    int r_;
    int lda_;
    int r_sub_;
    int ldsub_;

    const int * Index_;
    double * buffer_;
    double * SS_buff_;
    double * buff_S_;
    int    * ind_buff_;

    std::map<int, int > index_map_;
    std::set<int > low_0_index_;

    void   reduce_Schur_0_(double * const Sfull);
    bool   QR_of_Schur_complement_(const int n, const int r,
                                   double * const S, const int lds);
    void   inverse_reduce_(double * const S, int * const index_add);
};

///////////////////////////////////////////////////////////////////////////////

class ProximityEstimator {
public:

    ProximityEstimator();
    ~ProximityEstimator();

    // Check if rank of the matrix < rank_increment.
    bool  is_least_rank_detection(const double Eps);

    // Main criterion of comparision of the approximant and given matrix.
    bool  is_approximation_quite_good(const double Eps);

    // Updates every time.
    void  update_rank(const int rank);

    // Set up only once.
    void  set_inc_rank(const int incRank);
    void  set_UUhat(BaseArrayHolder<double> * const UUhat_in);
    void  set_VVhat(BaseArrayHolder<double> * const VVhat_in);
    void  set_u_sub(BaseArrayHolder<double> * const u_sub_in);
    void  set_v_sub(BaseArrayHolder<double> * const v_sub_in);
    void  set_Abox(BaseArrayHolder<double> * const Abox);
    void  set_buffers(BaseArrayHolder<double> * const buff_1,
                      BaseArrayHolder<double> * const buff_2,
                      BaseArrayHolder<double> * const buff_3);

    void  set_row_index(BaseArrayHolder<int> * const row_index_in);
    void  set_col_index(BaseArrayHolder<int> * const col_index_in);
    void  set_row_inc_index(BaseArrayHolder<int> * const row_inc_ind_in);
    void  set_col_inc_index(BaseArrayHolder<int> * const col_inc_ind_in);

private:

    int rank_;
    int rank_inc_;

    BaseArrayHolder<double> * UUhat_;    // U * U_hat.
    BaseArrayHolder<double> * VVhat_;    // V * V_hat.
    BaseArrayHolder<double> * u_sub_;    // Schur submatrix for maxvol search. All rows x rank cols.
    BaseArrayHolder<double> * v_sub_;    // Schur submatrix for maxvol search. All cols, rank rows.
    BaseArrayHolder<double> * Abox_;
    BaseArrayHolder<double> * buff_1_;   // double buffers for U and V
    BaseArrayHolder<double> * buff_2_;
    BaseArrayHolder<double> * buff_3_;

    // Maxvol indexes.
    BaseArrayHolder<int> * row_index_;
    BaseArrayHolder<int> * col_index_;
    BaseArrayHolder<int> * row_inc_index_;
    BaseArrayHolder<int> * col_inc_index_;

    bool   is_input_ok_();

    // Copy Abox from SchurCross2D to buffer array.
    bool   fill_up_Abox_(double * const Abox);

    void   matrix_x_Abox_(BaseArrayHolder<double> * const UUhat,
                          const double * const Abox,
                          const int size_of_A,
                          const enum CBLAS_TRANSPOSE TransA,
                          double * const buffer_rezlt);

    void   make_rows_selected_AAhatT_(BaseArrayHolder<double> * const AAhat,
                                      BaseArrayHolder<int> * const col_inc_index,
                                      double * const rezult_rsVVhatT);

    void   mult_AAhat_Abox_x_AAadd_(double * const UUhat_Abox, const int n,
                                    double * const rsVVhatT,
                                    double * const u1_approx);

    double compare_norm2_(BaseArrayHolder<double> * const a_sub,
                          double * const a_approx, const int n_rows_size,
                          double * const buff1, double * const buff2);
};

///////////////////////////////////////////////////////////////////////////////

#endif  // _MATRIX_TOOLS_H_

// End of the file

