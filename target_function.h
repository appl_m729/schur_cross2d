#ifndef _TARGET_FUNCTION_H_
#define _TARGET_FUNCTION_H_

///////////////////////////////////////////////////////////////////////////////

/*! Given function to be approximated on the rectangular grid.
 *  Calculates function value in a given point (i,j).
 */

class TargetFunction {
public:

    TargetFunction();
    virtual ~TargetFunction();

    // The value in given mesh point.
    virtual double value(const int i, const int j) const;

    // Density of mesh.
    bool   set_n_rows(const int n_rows);
    bool   set_m_cols(const int m_cols);

    // Mesh bounds.
    void   set_ax_left_bound(const double ax);
    void   set_bx_right_bound(const double bx);
    void   set_ay_left_bound(const double ay);
    void   set_by_right_bound(const double by);
    bool   are_bounds_setup_properly() const;

    // Matrix sizes.
    int    n_rows() const;   // x size of the rect
    int    m_cols() const;   // y size of the rect

    // Bounds of the definition rect.
    double ax_left_bound() const;
    double bx_right_bound() const;
    double ay_left_bound() const;
    double by_right_bound() const;

    // Steps: x_(i+1) - x_(i), mesh is homogenious (constant step)
    double hx() const;
    double hy() const;

    void   print_function_matrix(bool flag = true) const;
    void   print_initial_info() const;

private:

    int  n_rows_;
    int  m_cols_;

    double ax_left_bound_;
    double bx_right_bound_;
    double ay_left_bound_;
    double by_right_bound_;
};

///////////////////////////////////////////////////////////////////////////////

#endif  //  _TARGET_FUNCTION_H_

// End of the file

