#ifndef _MAXIMUM_VOLUME_ENGINE_H_
#define _MAXIMUM_VOLUME_ENGINE_H_

///////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

/*!  MaxvolEngine - finds maximum volume submatrix.
 *   One-thread implementation.
 *   When is called second or other time, the allocated
 *   memory is not freed (to avoi time consuming allocation).
 *
 *   The input matrix is supposed to be a column-like one.
 *   That is, the number of rows is greater then
 *   the number of columns.
 *   The data are ordered in row-like order (C style).
 *
 *   MaxvolEngine returns an unordered set of
 *   "good"-rows indeces of the length r_columns.
 *
 *  *** This module is successfully applicable for the
 *  row-like array format with a given capacity.
 *
 *   Version      pe-1.0.1, 24-10-2014
 *   Release 1.0  pe-1.0.3, 14-11-2014
 *
 *   Capacity bug fixed:
 *   Version      pe-1.0.4, 06-12-2014
 */

class MaxvolEngine : public Uncopyable {
public:

    MaxvolEngine();
    ~MaxvolEngine();

    // Call this routine before calling find_maximum_volume(...)
    bool   are_all_params_setup_properly() const;

    // Setup routines
    bool   set_tolerance(const double eps_tol = 1e-2);
    bool   set_rows_long_size(const int lnum_rows);
    bool   set_columns_short_size(const int snum_cols);
    bool   set_columns_capacity(const int capacity);  // leading dimension
    bool   set_iter_limit(const int max_iter = 2014);
    bool   set_matrix_pointer(const double * const mtrx_pntr);

    // One-thread implementation.
    bool   find_maximum_volume(int * const row_indexes);

private:

    // Input check flags
    bool     flag_n_row_;      // is the number of rows setup
    bool     flag_r_col_;      // is the number of columns setup
    bool     flag_p_mtrx_;     // is the matrix pointer setup

    // External input
    double   eps_tol_;         // tolerance for maxvol convergence
    int      n_row_;           // number of rows in column-like matrix
    int      r_col_;           // number of columns
    int      capacity_;        // leading dimension
    int      max_iter_;        // maximum iterations in maxvol
    const double * pA_;        // external pointer to the matrix

    // LU, Abox and B-transpose
    int      nr_buffer_size_;  // 2d-size of buffer array. >= 2d-size of A
    double * mtrx_data_;       // Matrix A is copied here
    double * mtrx_buff_;       // Buffer for matrix B = A^T.

    // Indeces
    int      ind_r_size_;      // 1d-size of buffer array for indexes (r)
    int      n_ind_size_;      // 1d-size of buffer array for indeces (n)
    int    * Ip_;              // Array of int indeces. The length is r
    int    * buff_ind_;        // Array of int indeces. The length is n

    bool   is_all_reallocation_ok_();  // (re)allocates memory for matrices
    void   copy_A_to_data_();
    void   transpose_A_to_Bt_();
    bool   decompose_A_in_PLU_();
    void   indexes_transform_(int * const toupd);
    bool   solve_A_Abox_inverse_();   // result = A * Abox^(-1)
    double get_local_argmax_(int & big_ind_1d, int & ib, int & jb);
};

///////////////////////////////////////////////////////////////////////////////

#endif  // _MAXIMUM_VOLUME_ENGINE_H_

// End of the file

